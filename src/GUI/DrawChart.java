/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Helper.AdditionalQueries;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author isalindrawan
 */
public class DrawChart {

    private ChartPanel chartPanel;

    public DrawChart() {

        JFreeChart barChart = ChartFactory.createBarChart(
                "GRAFIK PERBANDINGAN SELEKSI FITUR",
                "Seleksi Fitur",
                "Prosentase",
                createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(barChart);

        chartPanel.setPreferredSize(new java.awt.Dimension(600, 500));

        chartPanel.setVisible(true);
    }

    private CategoryDataset createDataset() {

        AdditionalQueries queries = new AdditionalQueries();

        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();

        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Accuracy", 4), "Tanpa Seleksi Fitur", "Accuracy");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Precision", 4), "Tanpa Seleksi Fitur", "Precision");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Recall", 4), "Tanpa Seleksi Fitur", "Recall");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Measure", 4), "Tanpa Seleksi Fitur", "F-Measure");

        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Accuracy", 1), "Seleksi Fitur 25%", "Accuracy");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Precision", 1), "Seleksi Fitur 25%", "Precision");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Recall", 1), "Seleksi Fitur 25%", "Recall");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Measure", 1), "Seleksi Fitur 25%", "F-Measure");

        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Accuracy", 2), "Seleksi Fitur 50%", "Accuracy");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Precision", 2), "Seleksi Fitur 50%", "Precision");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Recall", 2), "Seleksi Fitur 50%", "Recall");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Measure", 2), "Seleksi Fitur 50%", "F-Measure");

        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Accuracy", 3), "Seleksi Fitur 75%", "Accuracy");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Precision", 3), "Seleksi Fitur 75%", "Precision");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Recall", 3), "Seleksi Fitur 75%", "Recall");
        dataset.addValue(queries.getAccuracy("./temp/sentiment_model.db", "Measure", 3), "Seleksi Fitur 75%", "F-Measure");

        return dataset;
    }

    public ChartPanel getChart() {

        return chartPanel;
    }
}
