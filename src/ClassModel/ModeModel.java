/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassModel;

/**
 *
 * @author isalindrawan
 */
public class ModeModel {

    private final int posDoc, negDoc, posFreq, negFreq, unique;
    private final Double posPrior, negPrior, posLog, negLog;

    public ModeModel(int posDoc, int negDoc, int posFreq, int negFreq, int unique,
            Double posPrior, Double negPrior, Double posLog, Double negLog) {

        this.posDoc = posDoc;
        this.negDoc = negDoc;
        this.posFreq = posFreq;
        this.negFreq = negFreq;
        this.unique = unique;
        this.posPrior = posPrior;
        this.negPrior = negPrior;
        this.posLog = posLog;
        this.negLog = posLog;
    }

    public int getPosDoc() {

        return posDoc;
    }

    public int getNegDoc() {

        return negDoc;
    }

    public int getPosFreq() {

        return posFreq;
    }

    public int getNegFreq() {

        return negFreq;
    }

    public int getUnique() {

        return unique;
    }

    public Double getPosPrior() {

        return posPrior;
    }

    public Double getNegPrior() {

        return negPrior;
    }

    public Double getPosLog() {

        return posLog;
    }

    public Double getNegLog() {

        return negLog;
    }
}
