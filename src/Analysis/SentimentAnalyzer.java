package Analysis;

import PreProcess.ProcessCSV;
import PreProcess.Preprocessing;
import ClassModel.ReviewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JTextArea;

public class SentimentAnalyzer {

    private File testingCSV;

    private boolean multi;
    private ArrayList<ReviewModel> model;
    private ArrayList<String> term;
    private ArrayList<HashMap<String, Integer>> termCount;
    private String sentiment_db, prebuild_db, fileName;

    public SentimentAnalyzer(File testingCSV, String sentiment_db,
            String prebuild_db, JTextArea area, boolean multi, String file) {

        this.testingCSV = testingCSV;
        this.sentiment_db = sentiment_db;
        this.prebuild_db = prebuild_db;
        this.multi = multi;
        this.fileName = file;
        preTest();

        AnalyzerQueries analyzerQueries = new AnalyzerQueries(model, this.sentiment_db, this.prebuild_db, area, multi, fileName);

    }

    private void preTest() {

        import_csv();

        preprocess();
    }

    private void import_csv() {

        // konversi file csv menjadi arraylist class
        model = new ProcessCSV(testingCSV).getArrayList();
    }

    private void preprocess() {

        /*
            preprocessing dokumen review yang ada pada arraylist class
            selanjutnya di masukkan kembali kedalam arraylist asal
         */
        Preprocessing preprocess = new Preprocessing();
        for (int i = 0; i < model.size(); i++) {

            model.get(i).setReview(
                    new ArrayList<>(preprocess.analyze(
                            model.get(i).getReviewBefore().replaceAll("[^a-zA-Z\\s+]", " ")
                    ))
            );
        }
    }
}
