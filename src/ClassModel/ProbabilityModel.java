/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassModel;

/**
 *
 * @author isalindrawan
 */
public class ProbabilityModel {

    private final String term;
    private final int posFreq, negFreq;
    private final Double posProb, negProb, posLog, negLog;

    public ProbabilityModel(String term, int posFreq, int negFreq,
            Double posProb, Double negProb, Double posLog, Double negLog) {

        this.term = term;
        this.posFreq = posFreq;
        this.negFreq = negFreq;
        this.posProb = posProb;
        this.negProb = negProb;
        this.posLog = posLog;
        this.negLog = negLog;
    }

    public String getTerm() {

        return term;
    }

    public int getPosFreq() {

        return posFreq;
    }

    public int getNegFreq() {

        return negFreq;
    }

    public Double getPosProb() {

        return posProb;
    }

    public Double getNegProb() {

        return negProb;
    }

    public Double getPosLog() {

        return posLog;
    }

    public Double getNegLog() {

        return negLog;
    }
}
