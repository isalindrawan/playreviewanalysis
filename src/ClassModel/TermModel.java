/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassModel;

/**
 *
 * @author isalindrawan
 */
public class TermModel {

    private String term;
    private int positive, negative;
    private Double cs_positive, cs_negative, cs;

    public TermModel(String term, int positive, int negative, Double cs_positive,
            Double cs_negative, Double cs) {

        this.term = term;
        this.positive = positive;
        this.negative = negative;
        this.cs_positive = cs_positive;
        this.cs_negative = cs_negative;
        this.cs = cs;
    }

    public String getTerm() {

        return term;
    }

    public int getPositive() {

        return positive;
    }

    public int getNegative() {

        return negative;
    }

    public Double getCSPos() {

        return cs_positive;
    }

    public Double getCSNeg() {

        return cs_negative;
    }

    public Double getCS() {

        return cs;
    }
}
