package ClassModel;

import java.util.ArrayList;

public class ReviewModel {

    private final String appname;
    private ArrayList<String> review_after_list;
    private String review_before, review_after;
    private final int rate;
    private final int label;

    public ReviewModel(String appname, String review_before, int rate, int label) {

        this.appname = appname;
        this.rate = rate;
        this.label = label;
        this.review_before = review_before;

    }

    public String getAppname() {

        return appname;
    }

    public ArrayList<String> getReview() {

        return review_after_list;
    }

    public String getReviewBefore() {

        return review_before;
    }

    public String getReviewAfter() {

        return review_after;
    }

    public int getRate() {

        return rate;
    }

    public int getLabel() {

        return label;
    }

    public void setReview(ArrayList<String> review) {

        this.review_after_list = new ArrayList<>(review);
        this.review_after = String.join(" ", review);
    }
}
