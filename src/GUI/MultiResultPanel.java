/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Helper.AdditionalQueries;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author isalindrawan
 */
public class MultiResultPanel extends javax.swing.JPanel {

    /**
     * Creates new form resultPanel
     *
     * @param preset
     * @param training_db
     */
    public MultiResultPanel(boolean preset, String training_db, ArrayList<String> sentiment_db, ArrayList<File> files) {

        this.training_db = training_db;
        this.preset = preset;
        this.sentiment_db = sentiment_db;
        this.files = files;
        numberFormat = new DecimalFormat("#.00");

        initComponents();
        fillTrainDetail();
        fillDetail();

        fillCS100Table();
        fillCS25Table();
        fillCS50Table();
        fillCS75Table();

        fillAccuracyTable();
        fillPrecisionTable();
        fillRecallTable();
        fillMeasureTable();
        fillTimeTable();

    }

    public javax.swing.JPanel getPanel() {

        return this;
    }

    private void fillTrainDetail() {

        trainAppField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Application", false, null, null)));
        trainDocField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Review", false, null, null)));
        trainPositiveField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Review", true, "Review.Label", "1")));
        trainNegativeField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Review", true, "Review.Label", "0")));
        trainModelField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Model", false, null, null)));
        trainTermField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Term", false, null, null)));
    }

    private void fillDetail() {

        for (int i = 0; i < sentiment_db.size(); i++) {

            switch (i) {

                case 0:
                    fileNameField0.setText(" : " + files.get(i).getName());
                    testAppField0.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Application", false, null, null)));
                    testDocField0.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", false, null, null)));
                    testPositiveField0.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "1")));
                    testNegativeField0.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "0")));
                    break;
                case 1:
                    fileNameField1.setText(" : " + files.get(i).getName());
                    testAppField1.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Application", false, null, null)));
                    testDocField1.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", false, null, null)));
                    testPositiveField1.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "1")));
                    testNegativeField1.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "0")));
                    break;
                case 2:
                    fileNameField2.setText(" : " + files.get(i).getName());
                    testAppField2.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Application", false, null, null)));
                    testDocField2.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", false, null, null)));
                    testPositiveField2.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "1")));
                    testNegativeField2.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "0")));
                    break;
                case 3:
                    fileNameField3.setText(" : " + files.get(i).getName());
                    testAppField3.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Application", false, null, null)));
                    testDocField3.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", false, null, null)));
                    testPositiveField3.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "1")));
                    testNegativeField3.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "0")));
                    break;
                case 4:
                    fileNameField4.setText(" : " + files.get(i).getName());
                    testAppField4.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Application", false, null, null)));
                    testDocField4.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", false, null, null)));
                    testPositiveField4.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "1")));
                    testNegativeField4.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(sentiment_db.get(i), "Review", true, "Review.Label", "0")));
                    break;
            }
        }
    }

    private void fillCS100Table() {

        DefaultTableModel model = (DefaultTableModel) CS100Table.getModel();

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);

            String pos_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 4, 1));
            String neg_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 4, 0));
            String true_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 4, true));
            String false_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 4, false));

            model.addRow(new Object[]{files.get(i).getName(), true_predict, false_predict,
                pos_predict, neg_predict});
        }

        CS100Table.setModel(model);
        customizeTable(CS100Table);
    }

    private void fillCS25Table() {

        DefaultTableModel model = (DefaultTableModel) CS25Table.getModel();

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);

            String pos_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 1, 1));
            String neg_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 1, 0));
            String true_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 1, true));
            String false_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 1, false));

            model.addRow(new Object[]{files.get(i).getName(), true_predict, false_predict,
                pos_predict, neg_predict});
        }

        CS25Table.setModel(model);
        customizeTable(CS25Table);
    }

    private void fillCS50Table() {

        DefaultTableModel model = (DefaultTableModel) CS50Table.getModel();

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);

            String pos_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 2, 1));
            String neg_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 2, 0));
            String true_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 2, true));
            String false_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 2, false));

            model.addRow(new Object[]{files.get(i).getName(), true_predict, false_predict,
                pos_predict, neg_predict});
        }

        CS50Table.setModel(model);
        customizeTable(CS50Table);
    }

    private void fillCS75Table() {

        DefaultTableModel model = (DefaultTableModel) CS75Table.getModel();

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);

            String pos_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 3, 1));
            String neg_predict = String.valueOf(new AdditionalQueries().getCountPredictionLabel(db, 3, 0));
            String true_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 3, true));
            String false_predict = String.valueOf(new AdditionalQueries().getCountPrediction(db, 3, false));

            model.addRow(new Object[]{files.get(i).getName(), true_predict, false_predict,
                pos_predict, neg_predict});
        }

        CS75Table.setModel(model);
        customizeTable(CS75Table);
    }

    private void fillAccuracyTable() {

        DefaultTableModel model = (DefaultTableModel) accuracyTable.getModel();

        Double avg_1 = 0.0;
        Double avg_2 = 0.0;
        Double avg_3 = 0.0;
        Double avg_4 = 0.0;

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);
            Double accuracy_1 = new AdditionalQueries().getAccuracy(db, "Accuracy", 1);
            Double accuracy_2 = new AdditionalQueries().getAccuracy(db, "Accuracy", 2);
            Double accuracy_3 = new AdditionalQueries().getAccuracy(db, "Accuracy", 3);
            Double accuracy_4 = new AdditionalQueries().getAccuracy(db, "Accuracy", 4);

            avg_1 = avg_1 + accuracy_1;
            avg_2 = avg_2 + accuracy_2;
            avg_3 = avg_3 + accuracy_3;
            avg_4 = avg_4 + accuracy_4;

            model.addRow(new Object[]{files.get(i).getName(),
                String.valueOf(accuracy_4) + " %",
                String.valueOf(accuracy_1) + " %",
                String.valueOf(accuracy_2) + " %",
                String.valueOf(accuracy_3) + " %"
            });
        }

        avg_1 = BigDecimal.valueOf(avg_1 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_2 = BigDecimal.valueOf(avg_2 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_3 = BigDecimal.valueOf(avg_3 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_4 = BigDecimal.valueOf(avg_4 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();

        model.addRow(new Object[]{"", "", "", "", ""});
        model.addRow(new Object[]{"rata-rata",
            String.valueOf(avg_4) + " %",
            String.valueOf(avg_1) + " %",
            String.valueOf(avg_2) + " %",
            String.valueOf(avg_3) + " %"
        });

        accuracyTable.setModel(model);
        customizeTable(accuracyTable);
    }

    private void fillPrecisionTable() {

        DefaultTableModel model = (DefaultTableModel) precisionTable.getModel();

        Double avg_1 = 0.0;
        Double avg_2 = 0.0;
        Double avg_3 = 0.0;
        Double avg_4 = 0.0;

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);
            Double precision_1 = new AdditionalQueries().getAccuracy(db, "Precision", 1);
            Double precision_2 = new AdditionalQueries().getAccuracy(db, "Precision", 2);
            Double precision_3 = new AdditionalQueries().getAccuracy(db, "Precision", 3);
            Double precision_4 = new AdditionalQueries().getAccuracy(db, "Precision", 4);

            avg_1 = avg_1 + precision_1;
            avg_2 = avg_2 + precision_2;
            avg_3 = avg_3 + precision_3;
            avg_4 = avg_4 + precision_4;

            model.addRow(new Object[]{files.get(i).getName(),
                String.valueOf(precision_4) + " %",
                String.valueOf(precision_1) + " %",
                String.valueOf(precision_2) + " %",
                String.valueOf(precision_3) + " %"
            });
        }

        avg_1 = BigDecimal.valueOf(avg_1 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_2 = BigDecimal.valueOf(avg_2 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_3 = BigDecimal.valueOf(avg_3 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_4 = BigDecimal.valueOf(avg_4 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();

        model.addRow(new Object[]{"", "", "", "", ""});
        model.addRow(new Object[]{"rata-rata",
            String.valueOf(avg_4) + " %",
            String.valueOf(avg_1) + " %",
            String.valueOf(avg_2) + " %",
            String.valueOf(avg_3) + " %"
        });

        precisionTable.setModel(model);
        customizeTable(precisionTable);
    }

    private void fillRecallTable() {

        DefaultTableModel model = (DefaultTableModel) recallTable.getModel();

        Double avg_1 = 0.0;
        Double avg_2 = 0.0;
        Double avg_3 = 0.0;
        Double avg_4 = 0.0;

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);
            Double recall_1 = new AdditionalQueries().getAccuracy(db, "Recall", 1);
            Double recall_2 = new AdditionalQueries().getAccuracy(db, "Recall", 2);
            Double recall_3 = new AdditionalQueries().getAccuracy(db, "Recall", 3);
            Double recall_4 = new AdditionalQueries().getAccuracy(db, "Recall", 4);

            avg_1 = avg_1 + recall_1;
            avg_2 = avg_2 + recall_2;
            avg_3 = avg_3 + recall_3;
            avg_4 = avg_4 + recall_4;

            model.addRow(new Object[]{files.get(i).getName(),
                String.valueOf(recall_4) + " %",
                String.valueOf(recall_1) + " %",
                String.valueOf(recall_2) + " %",
                String.valueOf(recall_3) + " %"
            });
        }

        avg_1 = BigDecimal.valueOf(avg_1 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_2 = BigDecimal.valueOf(avg_2 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_3 = BigDecimal.valueOf(avg_3 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_4 = BigDecimal.valueOf(avg_4 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();

        model.addRow(new Object[]{"", "", "", "", ""});
        model.addRow(new Object[]{"rata-rata",
            String.valueOf(avg_4) + " %",
            String.valueOf(avg_1) + " %",
            String.valueOf(avg_2) + " %",
            String.valueOf(avg_3) + " %"
        });

        recallTable.setModel(model);
        customizeTable(recallTable);
    }

    private void fillMeasureTable() {

        DefaultTableModel model = (DefaultTableModel) measureTable.getModel();

        Double avg_1 = 0.0;
        Double avg_2 = 0.0;
        Double avg_3 = 0.0;
        Double avg_4 = 0.0;

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);
            Double measure_1 = new AdditionalQueries().getAccuracy(db, "Measure", 1);
            Double measure_2 = new AdditionalQueries().getAccuracy(db, "Measure", 2);
            Double measure_3 = new AdditionalQueries().getAccuracy(db, "Measure", 3);
            Double measure_4 = new AdditionalQueries().getAccuracy(db, "Measure", 4);

            avg_1 = avg_1 + measure_1;
            avg_2 = avg_2 + measure_2;
            avg_3 = avg_3 + measure_3;
            avg_4 = avg_4 + measure_4;

            model.addRow(new Object[]{files.get(i).getName(),
                String.valueOf(measure_4) + " %",
                String.valueOf(measure_1) + " %",
                String.valueOf(measure_2) + " %",
                String.valueOf(measure_3) + " %"
            });
        }

        avg_1 = BigDecimal.valueOf(avg_1 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_2 = BigDecimal.valueOf(avg_2 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_3 = BigDecimal.valueOf(avg_3 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_4 = BigDecimal.valueOf(avg_4 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();

        model.addRow(new Object[]{"", "", "", "", ""});
        model.addRow(new Object[]{"rata-rata",
            String.valueOf(avg_4) + " %",
            String.valueOf(avg_1) + " %",
            String.valueOf(avg_2) + " %",
            String.valueOf(avg_3) + " %"
        });

        measureTable.setModel(model);
        customizeTable(measureTable);
    }

    private void fillTimeTable() {

        DefaultTableModel model = (DefaultTableModel) timeTable.getModel();

        Double avg_1 = 0.0;
        Double avg_2 = 0.0;
        Double avg_3 = 0.0;
        Double avg_4 = 0.0;

        for (int i = 0; i < files.size(); i++) {

            String db = sentiment_db.get(i);
            Double time_1 = new AdditionalQueries().getAccuracy(db, "Time", 1);
            Double time_2 = new AdditionalQueries().getAccuracy(db, "Time", 2);
            Double time_3 = new AdditionalQueries().getAccuracy(db, "Time", 3);
            Double time_4 = new AdditionalQueries().getAccuracy(db, "Time", 4);

            avg_1 = avg_1 + time_1;
            avg_2 = avg_2 + time_2;
            avg_3 = avg_3 + time_3;
            avg_4 = avg_4 + time_4;

            model.addRow(new Object[]{files.get(i).getName(),
                String.valueOf(time_4) + " ms",
                String.valueOf(time_1) + " ms",
                String.valueOf(time_2) + " ms",
                String.valueOf(time_3) + " ms"
            });
        }

        avg_1 = BigDecimal.valueOf(avg_1 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_2 = BigDecimal.valueOf(avg_2 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_3 = BigDecimal.valueOf(avg_3 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();
        avg_4 = BigDecimal.valueOf(avg_4 / files.size()).setScale(2, RoundingMode.HALF_UP).doubleValue();

        model.addRow(new Object[]{"", "", "", "", ""});
        model.addRow(new Object[]{"rata-rata",
            String.valueOf(avg_4) + " ms",
            String.valueOf(avg_1) + " ms",
            String.valueOf(avg_2) + " ms",
            String.valueOf(avg_3) + " ms"
        });

        timeTable.setModel(model);
        customizeTable(timeTable);
    }

    private void customizeTable(javax.swing.JTable table) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        leftRenderer.setHorizontalAlignment(JLabel.LEFT);
        
        table.getColumnModel().getColumn(0).setCellRenderer(leftRenderer);
        
        for (int i = 1; i < table.getColumnCount(); i++) {

            table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
        }

        TableCellRenderer rendererFromHeader = table.getTableHeader().getDefaultRenderer();
        JLabel headerLabel = (JLabel) rendererFromHeader;
        headerLabel.setHorizontalAlignment(JLabel.CENTER);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainTabPane = new javax.swing.JTabbedPane();
        overviewScroller = new javax.swing.JScrollPane();
        overviewPanel = new javax.swing.JPanel();
        trainingPanel = new javax.swing.JPanel();
        trainAppLabel = new javax.swing.JLabel();
        trainDocLabel = new javax.swing.JLabel();
        trainPositiveLabel = new javax.swing.JLabel();
        trainNegativeLabel = new javax.swing.JLabel();
        trainModelLabel = new javax.swing.JLabel();
        trainTermLabel = new javax.swing.JLabel();
        trainAppField = new javax.swing.JLabel();
        trainDocField = new javax.swing.JLabel();
        trainPositiveField = new javax.swing.JLabel();
        trainNegativeField = new javax.swing.JLabel();
        trainModelField = new javax.swing.JLabel();
        trainTermField = new javax.swing.JLabel();
        testingPanel0 = new javax.swing.JPanel();
        testAppLabel0 = new javax.swing.JLabel();
        testDocLabel0 = new javax.swing.JLabel();
        testPositiveLabel0 = new javax.swing.JLabel();
        testNegativeLabel0 = new javax.swing.JLabel();
        testAppField0 = new javax.swing.JLabel();
        testDocField0 = new javax.swing.JLabel();
        testPositiveField0 = new javax.swing.JLabel();
        testNegativeField0 = new javax.swing.JLabel();
        fileNameLabel0 = new javax.swing.JLabel();
        fileNameField0 = new javax.swing.JLabel();
        testingPanel1 = new javax.swing.JPanel();
        testAppLabel1 = new javax.swing.JLabel();
        testDocLabel1 = new javax.swing.JLabel();
        testPositiveLabel1 = new javax.swing.JLabel();
        testNegativeLabel1 = new javax.swing.JLabel();
        testAppField1 = new javax.swing.JLabel();
        testDocField1 = new javax.swing.JLabel();
        testPositiveField1 = new javax.swing.JLabel();
        testNegativeField1 = new javax.swing.JLabel();
        fileNameLabel1 = new javax.swing.JLabel();
        fileNameField1 = new javax.swing.JLabel();
        testingPanel2 = new javax.swing.JPanel();
        testAppLabel2 = new javax.swing.JLabel();
        testDocLabel2 = new javax.swing.JLabel();
        testPositiveLabel2 = new javax.swing.JLabel();
        testNegativeLabel2 = new javax.swing.JLabel();
        testAppField2 = new javax.swing.JLabel();
        testDocField2 = new javax.swing.JLabel();
        testPositiveField2 = new javax.swing.JLabel();
        testNegativeField2 = new javax.swing.JLabel();
        fileNameLabel2 = new javax.swing.JLabel();
        fileNameField2 = new javax.swing.JLabel();
        testingPanel3 = new javax.swing.JPanel();
        testAppLabel3 = new javax.swing.JLabel();
        testDocLabel3 = new javax.swing.JLabel();
        testPositiveLabel3 = new javax.swing.JLabel();
        testNegativeLabel3 = new javax.swing.JLabel();
        testAppField3 = new javax.swing.JLabel();
        testDocField3 = new javax.swing.JLabel();
        testPositiveField3 = new javax.swing.JLabel();
        testNegativeField3 = new javax.swing.JLabel();
        fileNameLabel3 = new javax.swing.JLabel();
        fileNameField3 = new javax.swing.JLabel();
        testingPanel4 = new javax.swing.JPanel();
        testAppLabel4 = new javax.swing.JLabel();
        testDocLabel4 = new javax.swing.JLabel();
        testPositiveLabel4 = new javax.swing.JLabel();
        testNegativeLabel4 = new javax.swing.JLabel();
        testAppField4 = new javax.swing.JLabel();
        testDocField4 = new javax.swing.JLabel();
        testPositiveField4 = new javax.swing.JLabel();
        testNegativeField4 = new javax.swing.JLabel();
        fileNameLabel4 = new javax.swing.JLabel();
        fileNameField4 = new javax.swing.JLabel();
        detailScroller = new javax.swing.JScrollPane();
        detailPanel = new javax.swing.JPanel();
        CS100Panel = new javax.swing.JPanel();
        CS100Scroller = new javax.swing.JScrollPane();
        CS100Table = new javax.swing.JTable();
        CS25Panel = new javax.swing.JPanel();
        CS25Scroller = new javax.swing.JScrollPane();
        CS25Table = new javax.swing.JTable();
        CS50Panel = new javax.swing.JPanel();
        CS50Scroller = new javax.swing.JScrollPane();
        CS50Table = new javax.swing.JTable();
        CS75Panel = new javax.swing.JPanel();
        CS75Scroller = new javax.swing.JScrollPane();
        CS75Table = new javax.swing.JTable();
        performaceScroller = new javax.swing.JScrollPane();
        performancePanel = new javax.swing.JPanel();
        accuracyPanel = new javax.swing.JPanel();
        accuracyScroller = new javax.swing.JScrollPane();
        accuracyTable = new javax.swing.JTable();
        precisionPanel = new javax.swing.JPanel();
        precisionScroller = new javax.swing.JScrollPane();
        precisionTable = new javax.swing.JTable();
        recallPanel = new javax.swing.JPanel();
        recallScroller = new javax.swing.JScrollPane();
        recallTable = new javax.swing.JTable();
        measurePanel = new javax.swing.JPanel();
        measureScroller = new javax.swing.JScrollPane();
        measureTable = new javax.swing.JTable();
        timePanel = new javax.swing.JPanel();
        timeScroller = new javax.swing.JScrollPane();
        timeTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(734, 493));
        setPreferredSize(new java.awt.Dimension(734, 493));

        mainTabPane.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.SystemColor.controlHighlight));
        mainTabPane.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        mainTabPane.setMinimumSize(new java.awt.Dimension(734, 580));
        mainTabPane.setPreferredSize(new java.awt.Dimension(734, 580));

        overviewPanel.setLayout(new java.awt.GridLayout(6, 0, 0, 11));

        trainingPanel.setBackground(new java.awt.Color(255, 255, 255));
        trainingPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TRAINING", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        trainAppLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainAppLabel.setText("Aplikasi");
        trainAppLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainAppLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainAppLabel.setPreferredSize(new java.awt.Dimension(100, 15));

        trainDocLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainDocLabel.setText("Dokumen");
        trainDocLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainDocLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainDocLabel.setPreferredSize(new java.awt.Dimension(100, 15));

        trainPositiveLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainPositiveLabel.setText("Positif");
        trainPositiveLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainPositiveLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainPositiveLabel.setPreferredSize(new java.awt.Dimension(100, 15));

        trainNegativeLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainNegativeLabel.setText("Negatif");
        trainNegativeLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainNegativeLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainNegativeLabel.setPreferredSize(new java.awt.Dimension(100, 15));

        trainModelLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainModelLabel.setText("Model");
        trainModelLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainModelLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainModelLabel.setPreferredSize(new java.awt.Dimension(100, 15));

        trainTermLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainTermLabel.setText("Term");
        trainTermLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainTermLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainTermLabel.setPreferredSize(new java.awt.Dimension(100, 15));

        trainAppField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainAppField.setText(" : ");
        trainAppField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainAppField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainAppField.setPreferredSize(new java.awt.Dimension(206, 15));

        trainDocField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainDocField.setText(" : ");
        trainDocField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainDocField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainDocField.setPreferredSize(new java.awt.Dimension(206, 15));

        trainPositiveField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainPositiveField.setText(" : ");
        trainPositiveField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainPositiveField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainPositiveField.setPreferredSize(new java.awt.Dimension(206, 15));

        trainNegativeField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainNegativeField.setText(" : ");
        trainNegativeField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainNegativeField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainNegativeField.setPreferredSize(new java.awt.Dimension(206, 15));

        trainModelField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainModelField.setText(" : ");
        trainModelField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainModelField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainModelField.setPreferredSize(new java.awt.Dimension(206, 15));

        trainTermField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainTermField.setText(" : ");
        trainTermField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainTermField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainTermField.setPreferredSize(new java.awt.Dimension(206, 15));

        javax.swing.GroupLayout trainingPanelLayout = new javax.swing.GroupLayout(trainingPanel);
        trainingPanel.setLayout(trainingPanelLayout);
        trainingPanelLayout.setHorizontalGroup(
            trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(trainingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(trainDocLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                    .addComponent(trainPositiveLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainNegativeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainModelLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainTermLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainAppLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(trainTermField, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                    .addComponent(trainDocField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainPositiveField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainNegativeField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainModelField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(trainAppField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        trainingPanelLayout.setVerticalGroup(
            trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(trainingPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainAppLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trainAppField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainDocLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trainDocField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainPositiveLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trainPositiveField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainNegativeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trainNegativeField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainModelLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trainModelField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(trainingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(trainTermLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trainTermField, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        overviewPanel.add(trainingPanel);

        testingPanel0.setBackground(new java.awt.Color(255, 255, 255));
        testingPanel0.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TESTING 1", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        testAppLabel0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppLabel0.setText("Aplikasi");
        testAppLabel0.setMaximumSize(new java.awt.Dimension(100, 15));
        testAppLabel0.setMinimumSize(new java.awt.Dimension(100, 15));
        testAppLabel0.setPreferredSize(new java.awt.Dimension(100, 15));

        testDocLabel0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocLabel0.setText("Dokumen");
        testDocLabel0.setMaximumSize(new java.awt.Dimension(100, 15));
        testDocLabel0.setMinimumSize(new java.awt.Dimension(100, 15));
        testDocLabel0.setPreferredSize(new java.awt.Dimension(100, 15));

        testPositiveLabel0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveLabel0.setText("Positif");
        testPositiveLabel0.setMaximumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel0.setMinimumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel0.setPreferredSize(new java.awt.Dimension(100, 15));

        testNegativeLabel0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeLabel0.setText("Negatif");
        testNegativeLabel0.setMaximumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel0.setMinimumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel0.setPreferredSize(new java.awt.Dimension(100, 15));

        testAppField0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppField0.setText(" : ");
        testAppField0.setMaximumSize(new java.awt.Dimension(206, 15));
        testAppField0.setMinimumSize(new java.awt.Dimension(206, 15));
        testAppField0.setPreferredSize(new java.awt.Dimension(206, 15));

        testDocField0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocField0.setText(" : ");
        testDocField0.setMaximumSize(new java.awt.Dimension(206, 15));
        testDocField0.setMinimumSize(new java.awt.Dimension(206, 15));
        testDocField0.setPreferredSize(new java.awt.Dimension(206, 15));

        testPositiveField0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveField0.setText(" : ");
        testPositiveField0.setMaximumSize(new java.awt.Dimension(206, 15));
        testPositiveField0.setMinimumSize(new java.awt.Dimension(206, 15));
        testPositiveField0.setPreferredSize(new java.awt.Dimension(206, 15));

        testNegativeField0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeField0.setText(" : ");
        testNegativeField0.setMaximumSize(new java.awt.Dimension(206, 15));
        testNegativeField0.setMinimumSize(new java.awt.Dimension(206, 15));
        testNegativeField0.setPreferredSize(new java.awt.Dimension(206, 15));

        fileNameLabel0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameLabel0.setText("Nama File");
        fileNameLabel0.setMaximumSize(new java.awt.Dimension(100, 15));
        fileNameLabel0.setMinimumSize(new java.awt.Dimension(100, 15));
        fileNameLabel0.setPreferredSize(new java.awt.Dimension(100, 15));

        fileNameField0.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameField0.setText(" : ");
        fileNameField0.setMaximumSize(new java.awt.Dimension(206, 15));
        fileNameField0.setMinimumSize(new java.awt.Dimension(206, 15));
        fileNameField0.setPreferredSize(new java.awt.Dimension(206, 15));

        javax.swing.GroupLayout testingPanel0Layout = new javax.swing.GroupLayout(testingPanel0);
        testingPanel0.setLayout(testingPanel0Layout);
        testingPanel0Layout.setHorizontalGroup(
            testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testingPanel0Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(testingPanel0Layout.createSequentialGroup()
                        .addComponent(testNegativeLabel0, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(testNegativeField0, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE))
                    .addGroup(testingPanel0Layout.createSequentialGroup()
                        .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(testAppLabel0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testDocLabel0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testPositiveLabel0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(testingPanel0Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(testPositiveField0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(testDocField0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(testingPanel0Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(testAppField0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(testingPanel0Layout.createSequentialGroup()
                        .addComponent(fileNameLabel0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(fileNameField0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        testingPanel0Layout.setVerticalGroup(
            testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, testingPanel0Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileNameLabel0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileNameField0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testAppLabel0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testAppField0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testDocLabel0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testDocField0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testPositiveLabel0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testPositiveField0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testNegativeLabel0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testNegativeField0, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        overviewPanel.add(testingPanel0);

        testingPanel1.setBackground(new java.awt.Color(255, 255, 255));
        testingPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TESTING 2", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        testAppLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppLabel1.setText("Aplikasi");
        testAppLabel1.setMaximumSize(new java.awt.Dimension(100, 15));
        testAppLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        testAppLabel1.setPreferredSize(new java.awt.Dimension(100, 15));

        testDocLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocLabel1.setText("Dokumen");
        testDocLabel1.setMaximumSize(new java.awt.Dimension(100, 15));
        testDocLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        testDocLabel1.setPreferredSize(new java.awt.Dimension(100, 15));

        testPositiveLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveLabel1.setText("Positif");
        testPositiveLabel1.setMaximumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel1.setPreferredSize(new java.awt.Dimension(100, 15));

        testNegativeLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeLabel1.setText("Negatif");
        testNegativeLabel1.setMaximumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel1.setPreferredSize(new java.awt.Dimension(100, 15));

        testAppField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppField1.setText(" : ");
        testAppField1.setMaximumSize(new java.awt.Dimension(206, 15));
        testAppField1.setMinimumSize(new java.awt.Dimension(206, 15));
        testAppField1.setPreferredSize(new java.awt.Dimension(206, 15));

        testDocField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocField1.setText(" : ");
        testDocField1.setMaximumSize(new java.awt.Dimension(206, 15));
        testDocField1.setMinimumSize(new java.awt.Dimension(206, 15));
        testDocField1.setPreferredSize(new java.awt.Dimension(206, 15));

        testPositiveField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveField1.setText(" : ");
        testPositiveField1.setMaximumSize(new java.awt.Dimension(206, 15));
        testPositiveField1.setMinimumSize(new java.awt.Dimension(206, 15));
        testPositiveField1.setPreferredSize(new java.awt.Dimension(206, 15));

        testNegativeField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeField1.setText(" : ");
        testNegativeField1.setMaximumSize(new java.awt.Dimension(206, 15));
        testNegativeField1.setMinimumSize(new java.awt.Dimension(206, 15));
        testNegativeField1.setPreferredSize(new java.awt.Dimension(206, 15));

        fileNameLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameLabel1.setText("Nama File");
        fileNameLabel1.setMaximumSize(new java.awt.Dimension(100, 15));
        fileNameLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        fileNameLabel1.setPreferredSize(new java.awt.Dimension(100, 15));

        fileNameField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameField1.setText(" : ");
        fileNameField1.setMaximumSize(new java.awt.Dimension(206, 15));
        fileNameField1.setMinimumSize(new java.awt.Dimension(206, 15));
        fileNameField1.setPreferredSize(new java.awt.Dimension(206, 15));

        javax.swing.GroupLayout testingPanel1Layout = new javax.swing.GroupLayout(testingPanel1);
        testingPanel1.setLayout(testingPanel1Layout);
        testingPanel1Layout.setHorizontalGroup(
            testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testingPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(testingPanel1Layout.createSequentialGroup()
                        .addComponent(testNegativeLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(testNegativeField1, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE))
                    .addGroup(testingPanel1Layout.createSequentialGroup()
                        .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(testAppLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testDocLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testPositiveLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(testingPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(testPositiveField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(testDocField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(testingPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(testAppField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(testingPanel1Layout.createSequentialGroup()
                        .addComponent(fileNameLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(fileNameField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        testingPanel1Layout.setVerticalGroup(
            testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, testingPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileNameLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileNameField1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testAppLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testAppField1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testDocLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testDocField1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testPositiveLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testPositiveField1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testNegativeLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testNegativeField1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        overviewPanel.add(testingPanel1);

        testingPanel2.setBackground(new java.awt.Color(255, 255, 255));
        testingPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TESTING 3", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        testAppLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppLabel2.setText("Aplikasi");
        testAppLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        testAppLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        testAppLabel2.setPreferredSize(new java.awt.Dimension(100, 15));

        testDocLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocLabel2.setText("Dokumen");
        testDocLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        testDocLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        testDocLabel2.setPreferredSize(new java.awt.Dimension(100, 15));

        testPositiveLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveLabel2.setText("Positif");
        testPositiveLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel2.setPreferredSize(new java.awt.Dimension(100, 15));

        testNegativeLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeLabel2.setText("Negatif");
        testNegativeLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel2.setPreferredSize(new java.awt.Dimension(100, 15));

        testAppField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppField2.setText(" : ");
        testAppField2.setMaximumSize(new java.awt.Dimension(206, 15));
        testAppField2.setMinimumSize(new java.awt.Dimension(206, 15));
        testAppField2.setPreferredSize(new java.awt.Dimension(206, 15));

        testDocField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocField2.setText(" : ");
        testDocField2.setMaximumSize(new java.awt.Dimension(206, 15));
        testDocField2.setMinimumSize(new java.awt.Dimension(206, 15));
        testDocField2.setPreferredSize(new java.awt.Dimension(206, 15));

        testPositiveField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveField2.setText(" : ");
        testPositiveField2.setMaximumSize(new java.awt.Dimension(206, 15));
        testPositiveField2.setMinimumSize(new java.awt.Dimension(206, 15));
        testPositiveField2.setPreferredSize(new java.awt.Dimension(206, 15));

        testNegativeField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeField2.setText(" : ");
        testNegativeField2.setMaximumSize(new java.awt.Dimension(206, 15));
        testNegativeField2.setMinimumSize(new java.awt.Dimension(206, 15));
        testNegativeField2.setPreferredSize(new java.awt.Dimension(206, 15));

        fileNameLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameLabel2.setText("Nama File");
        fileNameLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        fileNameLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        fileNameLabel2.setPreferredSize(new java.awt.Dimension(100, 15));

        fileNameField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameField2.setText(" : ");
        fileNameField2.setMaximumSize(new java.awt.Dimension(206, 15));
        fileNameField2.setMinimumSize(new java.awt.Dimension(206, 15));
        fileNameField2.setPreferredSize(new java.awt.Dimension(206, 15));

        javax.swing.GroupLayout testingPanel2Layout = new javax.swing.GroupLayout(testingPanel2);
        testingPanel2.setLayout(testingPanel2Layout);
        testingPanel2Layout.setHorizontalGroup(
            testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testingPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(testingPanel2Layout.createSequentialGroup()
                        .addComponent(testNegativeLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(testNegativeField2, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE))
                    .addGroup(testingPanel2Layout.createSequentialGroup()
                        .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(testAppLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testDocLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testPositiveLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(testingPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(testPositiveField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(testDocField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(testingPanel2Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(testAppField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(testingPanel2Layout.createSequentialGroup()
                        .addComponent(fileNameLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(fileNameField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        testingPanel2Layout.setVerticalGroup(
            testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, testingPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileNameLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileNameField2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testAppLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testAppField2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testDocLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testDocField2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testPositiveLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testPositiveField2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testNegativeLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testNegativeField2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        overviewPanel.add(testingPanel2);

        testingPanel3.setBackground(new java.awt.Color(255, 255, 255));
        testingPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TESTING 4", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        testAppLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppLabel3.setText("Aplikasi");
        testAppLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        testAppLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        testAppLabel3.setPreferredSize(new java.awt.Dimension(100, 15));

        testDocLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocLabel3.setText("Dokumen");
        testDocLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        testDocLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        testDocLabel3.setPreferredSize(new java.awt.Dimension(100, 15));

        testPositiveLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveLabel3.setText("Positif");
        testPositiveLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel3.setPreferredSize(new java.awt.Dimension(100, 15));

        testNegativeLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeLabel3.setText("Negatif");
        testNegativeLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel3.setPreferredSize(new java.awt.Dimension(100, 15));

        testAppField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppField3.setText(" : ");
        testAppField3.setMaximumSize(new java.awt.Dimension(206, 15));
        testAppField3.setMinimumSize(new java.awt.Dimension(206, 15));
        testAppField3.setPreferredSize(new java.awt.Dimension(206, 15));

        testDocField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocField3.setText(" : ");
        testDocField3.setMaximumSize(new java.awt.Dimension(206, 15));
        testDocField3.setMinimumSize(new java.awt.Dimension(206, 15));
        testDocField3.setPreferredSize(new java.awt.Dimension(206, 15));

        testPositiveField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveField3.setText(" : ");
        testPositiveField3.setMaximumSize(new java.awt.Dimension(206, 15));
        testPositiveField3.setMinimumSize(new java.awt.Dimension(206, 15));
        testPositiveField3.setPreferredSize(new java.awt.Dimension(206, 15));

        testNegativeField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeField3.setText(" : ");
        testNegativeField3.setMaximumSize(new java.awt.Dimension(206, 15));
        testNegativeField3.setMinimumSize(new java.awt.Dimension(206, 15));
        testNegativeField3.setPreferredSize(new java.awt.Dimension(206, 15));

        fileNameLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameLabel3.setText("Nama File");
        fileNameLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        fileNameLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        fileNameLabel3.setPreferredSize(new java.awt.Dimension(100, 15));

        fileNameField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameField3.setText(" : ");
        fileNameField3.setMaximumSize(new java.awt.Dimension(206, 15));
        fileNameField3.setMinimumSize(new java.awt.Dimension(206, 15));
        fileNameField3.setPreferredSize(new java.awt.Dimension(206, 15));

        javax.swing.GroupLayout testingPanel3Layout = new javax.swing.GroupLayout(testingPanel3);
        testingPanel3.setLayout(testingPanel3Layout);
        testingPanel3Layout.setHorizontalGroup(
            testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testingPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(testingPanel3Layout.createSequentialGroup()
                        .addComponent(testNegativeLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(testNegativeField3, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE))
                    .addGroup(testingPanel3Layout.createSequentialGroup()
                        .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(testAppLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testDocLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testPositiveLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(testingPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(testPositiveField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(testDocField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(testingPanel3Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(testAppField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(testingPanel3Layout.createSequentialGroup()
                        .addComponent(fileNameLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(fileNameField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        testingPanel3Layout.setVerticalGroup(
            testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, testingPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileNameLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileNameField3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testAppLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testAppField3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testDocLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testDocField3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testPositiveLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testPositiveField3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testNegativeLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testNegativeField3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        overviewPanel.add(testingPanel3);

        testingPanel4.setBackground(new java.awt.Color(255, 255, 255));
        testingPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TESTING 5", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        testAppLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppLabel4.setText("Aplikasi");
        testAppLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        testAppLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        testAppLabel4.setPreferredSize(new java.awt.Dimension(100, 15));

        testDocLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocLabel4.setText("Dokumen");
        testDocLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        testDocLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        testDocLabel4.setPreferredSize(new java.awt.Dimension(100, 15));

        testPositiveLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveLabel4.setText("Positif");
        testPositiveLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel4.setPreferredSize(new java.awt.Dimension(100, 15));

        testNegativeLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeLabel4.setText("Negatif");
        testNegativeLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel4.setPreferredSize(new java.awt.Dimension(100, 15));

        testAppField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppField4.setText(" : ");
        testAppField4.setMaximumSize(new java.awt.Dimension(206, 15));
        testAppField4.setMinimumSize(new java.awt.Dimension(206, 15));
        testAppField4.setPreferredSize(new java.awt.Dimension(206, 15));

        testDocField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocField4.setText(" : ");
        testDocField4.setMaximumSize(new java.awt.Dimension(206, 15));
        testDocField4.setMinimumSize(new java.awt.Dimension(206, 15));
        testDocField4.setPreferredSize(new java.awt.Dimension(206, 15));

        testPositiveField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveField4.setText(" : ");
        testPositiveField4.setMaximumSize(new java.awt.Dimension(206, 15));
        testPositiveField4.setMinimumSize(new java.awt.Dimension(206, 15));
        testPositiveField4.setPreferredSize(new java.awt.Dimension(206, 15));

        testNegativeField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeField4.setText(" : ");
        testNegativeField4.setMaximumSize(new java.awt.Dimension(206, 15));
        testNegativeField4.setMinimumSize(new java.awt.Dimension(206, 15));
        testNegativeField4.setPreferredSize(new java.awt.Dimension(206, 15));

        fileNameLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameLabel4.setText("Nama File");
        fileNameLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        fileNameLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        fileNameLabel4.setPreferredSize(new java.awt.Dimension(100, 15));

        fileNameField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        fileNameField4.setText(" : ");
        fileNameField4.setMaximumSize(new java.awt.Dimension(206, 15));
        fileNameField4.setMinimumSize(new java.awt.Dimension(206, 15));
        fileNameField4.setPreferredSize(new java.awt.Dimension(206, 15));

        javax.swing.GroupLayout testingPanel4Layout = new javax.swing.GroupLayout(testingPanel4);
        testingPanel4.setLayout(testingPanel4Layout);
        testingPanel4Layout.setHorizontalGroup(
            testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testingPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(testingPanel4Layout.createSequentialGroup()
                        .addComponent(testNegativeLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(testNegativeField4, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE))
                    .addGroup(testingPanel4Layout.createSequentialGroup()
                        .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(testAppLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testDocLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(testPositiveLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(testingPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(testPositiveField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(testDocField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(testingPanel4Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(testAppField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(testingPanel4Layout.createSequentialGroup()
                        .addComponent(fileNameLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(fileNameField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        testingPanel4Layout.setVerticalGroup(
            testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, testingPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fileNameLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileNameField4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testAppLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testAppField4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testDocLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testDocField4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testPositiveLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testPositiveField4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(testingPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(testNegativeLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(testNegativeField4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
        );

        overviewPanel.add(testingPanel4);

        overviewScroller.setViewportView(overviewPanel);

        mainTabPane.addTab("Detail Dataset", overviewScroller);

        detailPanel.setLayout(new java.awt.GridLayout(4, 0, 0, 11));

        CS100Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS100Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TANPA SELEKSI FITUR", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        CS100Panel.setMinimumSize(new java.awt.Dimension(688, 166));
        CS100Panel.setPreferredSize(new java.awt.Dimension(688, 166));
        CS100Panel.setLayout(new java.awt.GridLayout(1, 0));

        CS100Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Benar", "Salah", "Positif", "Negatif"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        CS100Scroller.setViewportView(CS100Table);
        if (CS100Table.getColumnModel().getColumnCount() > 0) {
            CS100Table.getColumnModel().getColumn(0).setPreferredWidth(350);
            CS100Table.getColumnModel().getColumn(1).setResizable(false);
            CS100Table.getColumnModel().getColumn(2).setResizable(false);
            CS100Table.getColumnModel().getColumn(3).setResizable(false);
            CS100Table.getColumnModel().getColumn(4).setResizable(false);
        }

        CS100Panel.add(CS100Scroller);

        detailPanel.add(CS100Panel);

        CS25Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS25Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "25% SELEKSI FITUR", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        CS25Panel.setMinimumSize(new java.awt.Dimension(688, 166));
        CS25Panel.setPreferredSize(new java.awt.Dimension(688, 166));
        CS25Panel.setLayout(new java.awt.GridLayout(1, 0));

        CS25Table.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        CS25Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Benar", "Salah", "Positif", "Negatif"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        CS25Scroller.setViewportView(CS25Table);
        if (CS25Table.getColumnModel().getColumnCount() > 0) {
            CS25Table.getColumnModel().getColumn(0).setPreferredWidth(350);
            CS25Table.getColumnModel().getColumn(1).setResizable(false);
            CS25Table.getColumnModel().getColumn(2).setResizable(false);
            CS25Table.getColumnModel().getColumn(3).setResizable(false);
            CS25Table.getColumnModel().getColumn(4).setResizable(false);
        }

        CS25Panel.add(CS25Scroller);

        detailPanel.add(CS25Panel);

        CS50Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS50Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "50% SELEKSI FITUR", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        CS50Panel.setMinimumSize(new java.awt.Dimension(688, 166));
        CS50Panel.setPreferredSize(new java.awt.Dimension(688, 166));
        CS50Panel.setLayout(new java.awt.GridLayout(1, 0));

        CS50Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Benar", "Salah", "Positif", "Negatif"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        CS50Scroller.setViewportView(CS50Table);
        if (CS50Table.getColumnModel().getColumnCount() > 0) {
            CS50Table.getColumnModel().getColumn(0).setPreferredWidth(350);
            CS50Table.getColumnModel().getColumn(1).setResizable(false);
            CS50Table.getColumnModel().getColumn(2).setResizable(false);
            CS50Table.getColumnModel().getColumn(3).setResizable(false);
            CS50Table.getColumnModel().getColumn(4).setResizable(false);
        }

        CS50Panel.add(CS50Scroller);

        detailPanel.add(CS50Panel);

        CS75Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS75Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "75% SELEKSI FITUR", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        CS75Panel.setMinimumSize(new java.awt.Dimension(688, 166));
        CS75Panel.setPreferredSize(new java.awt.Dimension(688, 166));
        CS75Panel.setLayout(new java.awt.GridLayout(1, 0));

        CS75Table.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        CS75Table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Benar", "Salah", "Positif", "Negatif"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        CS75Scroller.setViewportView(CS75Table);
        if (CS75Table.getColumnModel().getColumnCount() > 0) {
            CS75Table.getColumnModel().getColumn(0).setPreferredWidth(350);
            CS75Table.getColumnModel().getColumn(1).setResizable(false);
            CS75Table.getColumnModel().getColumn(2).setResizable(false);
            CS75Table.getColumnModel().getColumn(3).setResizable(false);
            CS75Table.getColumnModel().getColumn(4).setResizable(false);
        }

        CS75Panel.add(CS75Scroller);

        detailPanel.add(CS75Panel);

        detailScroller.setViewportView(detailPanel);

        mainTabPane.addTab("Ringkasan Analisis", detailScroller);

        performancePanel.setLayout(new java.awt.GridLayout(5, 0, 0, 11));

        accuracyPanel.setBackground(new java.awt.Color(255, 255, 255));
        accuracyPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "AKURASI", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        accuracyPanel.setMinimumSize(new java.awt.Dimension(688, 166));
        accuracyPanel.setPreferredSize(new java.awt.Dimension(688, 166));
        accuracyPanel.setLayout(new java.awt.GridLayout(1, 0));

        accuracyTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Tanpa Seleksi Fitur", "25% Seleksi Fitur", "50% Seleksi Fitur", "75% Seleksi Fitur"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        accuracyScroller.setViewportView(accuracyTable);
        if (accuracyTable.getColumnModel().getColumnCount() > 0) {
            accuracyTable.getColumnModel().getColumn(0).setPreferredWidth(260);
            accuracyTable.getColumnModel().getColumn(1).setResizable(false);
            accuracyTable.getColumnModel().getColumn(2).setResizable(false);
            accuracyTable.getColumnModel().getColumn(3).setResizable(false);
            accuracyTable.getColumnModel().getColumn(4).setResizable(false);
        }

        accuracyPanel.add(accuracyScroller);

        performancePanel.add(accuracyPanel);

        precisionPanel.setBackground(new java.awt.Color(255, 255, 255));
        precisionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PRESISI", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        precisionPanel.setMinimumSize(new java.awt.Dimension(688, 166));
        precisionPanel.setPreferredSize(new java.awt.Dimension(688, 166));
        precisionPanel.setLayout(new java.awt.GridLayout(1, 0));

        precisionTable.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Tanpa Seleksi Fitur", "25% Seleksi Fitur", "50% Seleksi Fitur", "75% Seleksi Fitur"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        precisionScroller.setViewportView(precisionTable);
        if (precisionTable.getColumnModel().getColumnCount() > 0) {
            precisionTable.getColumnModel().getColumn(0).setPreferredWidth(260);
            precisionTable.getColumnModel().getColumn(1).setResizable(false);
            precisionTable.getColumnModel().getColumn(2).setResizable(false);
            precisionTable.getColumnModel().getColumn(3).setResizable(false);
            precisionTable.getColumnModel().getColumn(4).setResizable(false);
        }

        precisionPanel.add(precisionScroller);

        performancePanel.add(precisionPanel);

        recallPanel.setBackground(new java.awt.Color(255, 255, 255));
        recallPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "RECALL", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        recallPanel.setMinimumSize(new java.awt.Dimension(688, 166));
        recallPanel.setPreferredSize(new java.awt.Dimension(688, 166));
        recallPanel.setLayout(new java.awt.GridLayout(1, 0));

        recallTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Tanpa Seleksi Fitur", "25% Seleksi Fitur", "50% Seleksi Fitur", "75% Seleksi Fitur"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        recallScroller.setViewportView(recallTable);
        if (recallTable.getColumnModel().getColumnCount() > 0) {
            recallTable.getColumnModel().getColumn(0).setPreferredWidth(260);
            recallTable.getColumnModel().getColumn(1).setResizable(false);
            recallTable.getColumnModel().getColumn(2).setResizable(false);
            recallTable.getColumnModel().getColumn(3).setResizable(false);
            recallTable.getColumnModel().getColumn(4).setResizable(false);
        }

        recallPanel.add(recallScroller);

        performancePanel.add(recallPanel);

        measurePanel.setBackground(new java.awt.Color(255, 255, 255));
        measurePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "F-MEASURE", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        measurePanel.setMinimumSize(new java.awt.Dimension(688, 166));
        measurePanel.setPreferredSize(new java.awt.Dimension(688, 166));
        measurePanel.setLayout(new java.awt.GridLayout(1, 0));

        measureTable.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measureTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Tanpa Seleksi Fitur", "25% Seleksi Fitur", "50% Seleksi Fitur", "75% Seleksi Fitur"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        measureScroller.setViewportView(measureTable);
        if (measureTable.getColumnModel().getColumnCount() > 0) {
            measureTable.getColumnModel().getColumn(0).setPreferredWidth(260);
            measureTable.getColumnModel().getColumn(1).setResizable(false);
            measureTable.getColumnModel().getColumn(2).setResizable(false);
            measureTable.getColumnModel().getColumn(3).setResizable(false);
            measureTable.getColumnModel().getColumn(4).setResizable(false);
        }

        measurePanel.add(measureScroller);

        performancePanel.add(measurePanel);

        timePanel.setBackground(new java.awt.Color(255, 255, 255));
        timePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "WAKTU EKSEKUSI", javax.swing.border.TitledBorder.RIGHT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N
        timePanel.setMinimumSize(new java.awt.Dimension(688, 166));
        timePanel.setPreferredSize(new java.awt.Dimension(688, 166));
        timePanel.setLayout(new java.awt.GridLayout(1, 0));

        timeTable.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Tanpa Seleksi Fitur", "25% Seleksi Fitur", "50% Seleksi Fitur", "75% Seleksi Fitur"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        timeScroller.setViewportView(timeTable);
        if (timeTable.getColumnModel().getColumnCount() > 0) {
            timeTable.getColumnModel().getColumn(0).setPreferredWidth(260);
            timeTable.getColumnModel().getColumn(1).setResizable(false);
            timeTable.getColumnModel().getColumn(2).setResizable(false);
            timeTable.getColumnModel().getColumn(3).setResizable(false);
            timeTable.getColumnModel().getColumn(4).setResizable(false);
        }

        timePanel.add(timeScroller);

        performancePanel.add(timePanel);

        performaceScroller.setViewportView(performancePanel);

        mainTabPane.addTab("Perbandingan Performa", performaceScroller);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainTabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 714, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainTabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private DecimalFormat numberFormat;
    private String training_db;
    private boolean preset;
    private ArrayList<String> sentiment_db;
    private ArrayList<File> files;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel CS100Panel;
    private javax.swing.JScrollPane CS100Scroller;
    private javax.swing.JTable CS100Table;
    private javax.swing.JPanel CS25Panel;
    private javax.swing.JScrollPane CS25Scroller;
    private javax.swing.JTable CS25Table;
    private javax.swing.JPanel CS50Panel;
    private javax.swing.JScrollPane CS50Scroller;
    private javax.swing.JTable CS50Table;
    private javax.swing.JPanel CS75Panel;
    private javax.swing.JScrollPane CS75Scroller;
    private javax.swing.JTable CS75Table;
    private javax.swing.JPanel accuracyPanel;
    private javax.swing.JScrollPane accuracyScroller;
    private javax.swing.JTable accuracyTable;
    private javax.swing.JPanel detailPanel;
    private javax.swing.JScrollPane detailScroller;
    private javax.swing.JLabel fileNameField0;
    private javax.swing.JLabel fileNameField1;
    private javax.swing.JLabel fileNameField2;
    private javax.swing.JLabel fileNameField3;
    private javax.swing.JLabel fileNameField4;
    private javax.swing.JLabel fileNameLabel0;
    private javax.swing.JLabel fileNameLabel1;
    private javax.swing.JLabel fileNameLabel2;
    private javax.swing.JLabel fileNameLabel3;
    private javax.swing.JLabel fileNameLabel4;
    private javax.swing.JTabbedPane mainTabPane;
    private javax.swing.JPanel measurePanel;
    private javax.swing.JScrollPane measureScroller;
    private javax.swing.JTable measureTable;
    private javax.swing.JPanel overviewPanel;
    private javax.swing.JScrollPane overviewScroller;
    private javax.swing.JScrollPane performaceScroller;
    private javax.swing.JPanel performancePanel;
    private javax.swing.JPanel precisionPanel;
    private javax.swing.JScrollPane precisionScroller;
    private javax.swing.JTable precisionTable;
    private javax.swing.JPanel recallPanel;
    private javax.swing.JScrollPane recallScroller;
    private javax.swing.JTable recallTable;
    private javax.swing.JLabel testAppField0;
    private javax.swing.JLabel testAppField1;
    private javax.swing.JLabel testAppField2;
    private javax.swing.JLabel testAppField3;
    private javax.swing.JLabel testAppField4;
    private javax.swing.JLabel testAppLabel0;
    private javax.swing.JLabel testAppLabel1;
    private javax.swing.JLabel testAppLabel2;
    private javax.swing.JLabel testAppLabel3;
    private javax.swing.JLabel testAppLabel4;
    private javax.swing.JLabel testDocField0;
    private javax.swing.JLabel testDocField1;
    private javax.swing.JLabel testDocField2;
    private javax.swing.JLabel testDocField3;
    private javax.swing.JLabel testDocField4;
    private javax.swing.JLabel testDocLabel0;
    private javax.swing.JLabel testDocLabel1;
    private javax.swing.JLabel testDocLabel2;
    private javax.swing.JLabel testDocLabel3;
    private javax.swing.JLabel testDocLabel4;
    private javax.swing.JLabel testNegativeField0;
    private javax.swing.JLabel testNegativeField1;
    private javax.swing.JLabel testNegativeField2;
    private javax.swing.JLabel testNegativeField3;
    private javax.swing.JLabel testNegativeField4;
    private javax.swing.JLabel testNegativeLabel0;
    private javax.swing.JLabel testNegativeLabel1;
    private javax.swing.JLabel testNegativeLabel2;
    private javax.swing.JLabel testNegativeLabel3;
    private javax.swing.JLabel testNegativeLabel4;
    private javax.swing.JLabel testPositiveField0;
    private javax.swing.JLabel testPositiveField1;
    private javax.swing.JLabel testPositiveField2;
    private javax.swing.JLabel testPositiveField3;
    private javax.swing.JLabel testPositiveField4;
    private javax.swing.JLabel testPositiveLabel0;
    private javax.swing.JLabel testPositiveLabel1;
    private javax.swing.JLabel testPositiveLabel2;
    private javax.swing.JLabel testPositiveLabel3;
    private javax.swing.JLabel testPositiveLabel4;
    private javax.swing.JPanel testingPanel0;
    private javax.swing.JPanel testingPanel1;
    private javax.swing.JPanel testingPanel2;
    private javax.swing.JPanel testingPanel3;
    private javax.swing.JPanel testingPanel4;
    private javax.swing.JPanel timePanel;
    private javax.swing.JScrollPane timeScroller;
    private javax.swing.JTable timeTable;
    private javax.swing.JLabel trainAppField;
    private javax.swing.JLabel trainAppLabel;
    private javax.swing.JLabel trainDocField;
    private javax.swing.JLabel trainDocLabel;
    private javax.swing.JLabel trainModelField;
    private javax.swing.JLabel trainModelLabel;
    private javax.swing.JLabel trainNegativeField;
    private javax.swing.JLabel trainNegativeLabel;
    private javax.swing.JLabel trainPositiveField;
    private javax.swing.JLabel trainPositiveLabel;
    private javax.swing.JLabel trainTermField;
    private javax.swing.JLabel trainTermLabel;
    private javax.swing.JPanel trainingPanel;
    // End of variables declaration//GEN-END:variables
}
