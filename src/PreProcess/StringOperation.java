
package PreProcess;

import ClassModel.ReviewModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;

public class StringOperation {

    private ArrayList<ReviewModel> data;

    public StringOperation(ArrayList<ReviewModel> data) {

        this.data = new ArrayList<>(data);
    }

    public StringOperation() {

    }

    // generate unique term from all documents
    public ArrayList<String> generateSingleTerm() {

        ArrayList<String> term = new ArrayList<>();
        LinkedHashSet<String> set = new LinkedHashSet<>();

        data.forEach((temp) -> {
            
            term.addAll(temp.getReview());
        });

        set.addAll(term);

        term.clear();

        term.addAll(set);

        return term;
    }

    // count total of every single words in documents
    public HashMap<String, Integer> countTermInReview(ArrayList<String> param) {

        HashMap<String, Integer> map = new HashMap<>();

        ArrayList<String> review = new ArrayList<>(param);

        LinkedHashSet<String> set;

        set = new LinkedHashSet<>(review);

        review.clear();

        review = new ArrayList<>(set);

        for (String term : review) {

            map.put(term, Collections.frequency(param, term));
        }

        return map;
    }
}
