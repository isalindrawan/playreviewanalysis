/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author isalindrawan
 */
public class MultiGraphPanel extends javax.swing.JPanel {

    /**
     * Creates new form resultPanel
     *
     * @param preset
     * @param training_db
     */
    public MultiGraphPanel(ArrayList<String> db, ArrayList<File> files) {

        this.db = db;
        this.files = files;

        initComponents();
        generateGraph();

    }

    private void generateGraph() {

        javax.swing.JScrollPane chartScroller4 = new javax.swing.JScrollPane();
        MultiDrawChart chart4 = new MultiDrawChart(db, files, 4, "Tanpa Seleksi Fitur");
        chartScroller4.setViewportView(chart4.getChart());
        mainTabPane.add("Tanpa Seleksi Fitur", chartScroller4);

        javax.swing.JScrollPane chartScroller1 = new javax.swing.JScrollPane();
        MultiDrawChart chart1 = new MultiDrawChart(db, files, 1, "25% Seleksi Fitur");
        chartScroller1.setViewportView(chart1.getChart());
        mainTabPane.add("25% Seleksi Fitur", chartScroller1);

        javax.swing.JScrollPane chartScroller2 = new javax.swing.JScrollPane();
        MultiDrawChart chart2 = new MultiDrawChart(db, files, 2, "50% Seleksi Fitur");
        chartScroller2.setViewportView(chart2.getChart());
        mainTabPane.add("50% Seleksi Fitur", chartScroller2);

        javax.swing.JScrollPane chartScroller3 = new javax.swing.JScrollPane();
        MultiDrawChart chart3 = new MultiDrawChart(db, files, 3, "75% Seleksi Fitur");
        chartScroller3.setViewportView(chart3.getChart());
        mainTabPane.add("75% Seleksi Fitur", chartScroller3);

        mainTabPane.setSelectedIndex(mainTabPane.indexOfTab("Tanpa Seleksi Fitur"));
    }

    public javax.swing.JPanel getPanel() {

        return this;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainTabPane = new javax.swing.JTabbedPane();

        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(734, 493));
        setPreferredSize(new java.awt.Dimension(734, 493));
        setLayout(new java.awt.GridLayout(1, 0));

        mainTabPane.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.SystemColor.controlHighlight));
        mainTabPane.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        mainTabPane.setMinimumSize(new java.awt.Dimension(734, 580));
        mainTabPane.setPreferredSize(new java.awt.Dimension(734, 580));
        add(mainTabPane);
    }// </editor-fold>//GEN-END:initComponents

    private ArrayList<String> db;
    private ArrayList<File> files;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane mainTabPane;
    // End of variables declaration//GEN-END:variables
}
