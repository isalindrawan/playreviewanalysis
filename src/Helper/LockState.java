/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isalindrawan
 */
public class LockState {

    public void lock(int number) {

        File create = new File("./temp/lock_" + number + ".file");

        try {

            create.createNewFile();

        } catch (IOException ex) {
            Logger.getLogger(PrepareDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void unlock(int number) {

        File file = new File("./temp/lock_" + number + ".file");

        file.delete();
    }
}
