package Helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DButils {

    public Connection connect(String db) {

        Connection connect = null;

        try {

            connect = DriverManager.getConnection("jdbc:sqlite:" + db);

        } catch (SQLException ex) {
            Logger.getLogger(DButils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return connect;
    }

    public void disconnect(Connection connect) {

        if (connect != null) {

            try {

                connect.close();

            } catch (SQLException ex) {
                Logger.getLogger(DButils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void closeResult(ResultSet rs) {

        if (rs != null) {

            try {

                rs.close();

            } catch (SQLException ex) {
                Logger.getLogger(DButils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void closeStatement(Statement state) {

        if (state != null) {

            try {

                state.close();

            } catch (SQLException ex) {
                Logger.getLogger(DButils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void closePS(PreparedStatement ps) {

        if (ps != null) {

            try {

                ps.close();

            } catch (SQLException ex) {
                Logger.getLogger(DButils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
