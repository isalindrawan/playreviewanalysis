/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassModel;

/**
 *
 * @author isalindrawan
 */
public class ResultModel {

    private String review, label, prediction;
    private Double positive;
    private Double negative;

    public ResultModel(String review, Double negative, Double positive, String label, String prediction) {

        this.review = review;
        this.label = label;
        this.prediction = prediction;
        this.positive = positive;
        this.negative = negative;
    }

    public String getReview() {

        return review;
    }

    public String getLabel() {

        return label;
    }

    public String getPrediction() {

        return prediction;
    }

    public Double getPositive() {

        return positive;
    }

    public Double getNegative() {

        return negative;
    }
}
