package TrainingClassifier;

import static java.lang.Math.pow;

public class ChiSquare {

    private int N, AB, BA, CD, DC;
    private Double CS_Positive, CS_Negative, CS;

    public ChiSquare(int N, int AB, int BA, int CD, int DC) {

        CS_Positive = 0.0;
        CS_Negative = 0.0;
        CS = 0.0;

        this.N = N;
        this.AB = AB;
        this.BA = BA;
        this.CD = CD;
        this.DC = DC;

        CS_Positive = N * (pow((AB * DC) - (CD * BA), 2)) / ((AB + CD) * (BA + DC) * (CD + DC));
        CS_Negative = N * (pow((BA * CD) - (DC * AB), 2)) / ((BA + DC) * (AB + CD) * (DC + CD));
        CS = CS_Positive + CS_Negative;
    }

    public Double getCS_Positive() {

        return CS_Positive;
    }

    public Double getCS_Negative() {

        return CS_Negative;
    }

    public Double getCS() {

        return CS;
    }
}
