/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Helper.AdditionalQueries;
import java.io.File;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author isalindrawan
 */
public class MultiDrawChart {

    private ChartPanel chartPanel;
    private ArrayList<String> db;
    private ArrayList<File> files;
    private int model;

    public MultiDrawChart(ArrayList<String> db, ArrayList<File> files, int model, String title) {

        this.db = db;
        this.files = files;
        this.model = model;

        JFreeChart barChart = ChartFactory.createBarChart(
                title,
                "File",
                "Prosentase",
                createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);

        chartPanel = new ChartPanel(barChart);

        chartPanel.setPreferredSize(new java.awt.Dimension(600, 500));

        chartPanel.setVisible(true);
    }

    private CategoryDataset createDataset() {

        AdditionalQueries queries = new AdditionalQueries();

        final DefaultCategoryDataset dataset
                = new DefaultCategoryDataset();

        for (int i = 0; i < db.size(); i++) {

            dataset.addValue(queries.getAccuracy(db.get(i), "Accuracy", model), files.get(i).getName(), "Accuracy");
            dataset.addValue(queries.getAccuracy(db.get(i), "Precision", model), files.get(i).getName(), "Precision");
            dataset.addValue(queries.getAccuracy(db.get(i), "Recall", model), files.get(i).getName(), "Recall");
            dataset.addValue(queries.getAccuracy(db.get(i), "Measure", model), files.get(i).getName(), "F-Measure");
        }

        return dataset;
    }

    public ChartPanel getChart() {

        return chartPanel;
    }
}
