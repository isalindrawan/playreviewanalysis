package TrainingClassifier;

import Helper.DButils;
import ClassModel.ReviewModel;
import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

public final class TrainQueries {

    private String user_db;
    private HashMap<Integer, ArrayList<Integer>> termPercentage;
    private ArrayList<Integer> limitCount, limit;

    public TrainQueries(ArrayList<ReviewModel> model, ArrayList<String> term,
            ArrayList<HashMap<String, Integer>> termCount,
            ArrayList<Integer> limit, String user_db, JTextArea area) {

        this.user_db = user_db;
        this.limit = new ArrayList<>(limit);

        clearTables();

        try {

            long start = System.currentTimeMillis();

            area.append("MULAI PELATIHAN DATA\n\n");
            Thread.sleep(500);

            insertApp(model);
            area.append("[SELESAI] simpan app ke DB\n");
            Thread.sleep(500);

            insertReview(model);
            area.append("[SELESAI] simpan review ke DB\n");
            Thread.sleep(500);

            insertTerm(term);
            area.append("[SELESAI] simpan term ke DB\n");
            Thread.sleep(500);

            insertReviewTerm(termCount);
            area.append("[SELESAI] koneksikan review dan term\n");
            Thread.sleep(500);

            insertChiSquare();
            area.append("[SELESAI] kalkulasi nilai chi square\n");
            Thread.sleep(500);

            generateTermModel();
            area.append("[SELESAI] bentuk model\n");
            Thread.sleep(500);

            insertModel();
            area.append("[SELESAI] simpan model ke DB\n");
            Thread.sleep(500);

            insertTermProbModel();
            area.append("[SELESAI] koneksikan term dan model\n");
            Thread.sleep(500);

            insertProbability();
            area.append("[SELESAI] kalkulasi probabilitas\n");
            Thread.sleep(500);

            updateTermProbModel();
            area.append("[SELESAI] koneksikan term dan probabilitas\n\n");
            Thread.sleep(500);

            long end = System.currentTimeMillis();

            area.append("PELATIHAN SUKSES \n");
            area.append("dalam waktu " + TimeUnit.MILLISECONDS.toSeconds(end - start) + " detik\n\n");

            Thread.sleep(500);

        } catch (InterruptedException ex) {
            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public TrainQueries(String user_db) {

        this.user_db = user_db;
    }

    private void clearTables() {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        String[] tables = {"Term_Probability_Model", "Model", "Probability",
            "Review_Term", "Term", "Chi",
            "Review", "Application"};

        try {

            state = connect.createStatement();

            for (String table : tables) {

                String query = "delete from " + table;
                state.addBatch(query);
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    private void generateTermModel() {

        GenerateModelTerm generate = new GenerateModelTerm(limit, user_db);

        termPercentage = new HashMap<>(generate.getModelTerm());
        limitCount = new ArrayList<>(generate.getTermLimit());
    }

    // insert application table
    private void insertApp(ArrayList<ReviewModel> param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        PreparedStatement ps = null;

        try {

            // menghapus duplikasi nama aplikasi pada tiap data
            ArrayList<String> name = new ArrayList<>();
            LinkedHashSet<String> set;

            for (ReviewModel temp : param) {

                name.add(temp.getAppname());
            }

            // konversi ke hashset untuk menghapus duplikasi pada arraylist
            // app name
            set = new LinkedHashSet<>(name);
            name = new ArrayList<>(set);

            // inisiasi query
            String query = "insert into Application (ID, Name) values (NULL, ?)";
            ps = connect.prepareStatement(query);

            // menambahkan query untuk tiap nama app, untuk di eksekusi
            // secara bersamaan / batch
            for (String temp : name) {

                ps.setString(1, temp);
                ps.addBatch();
            }

            // execute batch queries
            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.disconnect(connect);
            utils.closePS(ps);
        }
    }

    // insert review table
    private void insertReview(ArrayList<ReviewModel> param) {

        /*
                menambahkan query insert dengan value review, rate, label
                dan id aplikasi yang di dapat melalui sub query
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        try {

            state = connect.createStatement();

            for (ReviewModel temp : param) {

                String query
                        = "insert into Review values (NULL, '" + temp.getReviewAfter()
                        + "', " + temp.getRate() + ", " + temp.getLabel()
                        + ", (select ID from Application where Name = '"
                        + temp.getAppname() + "'))";

                state.addBatch(query);
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    // insert term table
    private void insertTerm(ArrayList<String> param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        PreparedStatement ps = null;

        try {

            // inisiasi query insert
            String query = "insert into Term (ID, Term) values (NULL, ?)";
            ps = connect.prepareStatement(query);

            // add queries to batch
            for (String temp : param) {

                ps.setString(1, temp);
                ps.addBatch();
            }

            // execute batch queries
            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }

    // insert review term table
    private void insertReviewTerm(ArrayList<HashMap<String, Integer>> param) {

        /*
                insert tabel penghubung antara review dan kata
                hashmap berisi kata dan jumlah, sedangkan arraylist
                id menandakan id review, sebagai contoh insert
                isi dengan
                id review 1 dengan kata A dengan jumlah 5
                id review 1 dengan kata B dengan jumlah 2
                dst.
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        ArrayList<Integer> ID = new ArrayList<>(getID("Review"));

        try {

            state = connect.createStatement();

            int counter = 0;

            // add query with sub query to batch
            for (HashMap<String, Integer> temp : param) {

                Set set = temp.entrySet();
                Iterator iterator = set.iterator();

                while (iterator.hasNext()) {

                    Map.Entry entry = (Map.Entry) iterator.next();

                    String query = "insert into Review_Term values (NULL," + ID.get(counter)
                            + ", (select ID from Term where Term = '"
                            + entry.getKey() + "'), " + entry.getValue() + ")";

                    state.addBatch(query);
                }

                // execute and clear batch
                state.executeBatch();
                state.clearBatch();

                counter++;
            }

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    // insert chi square
    private void insertChiSquare() {

        /*
            untuk melakukan operasi insert pada tabel chi square
            pertama-tama ambil semua id kata pada tabel term
            selanjutnya dapatkan masing masing total dokumen pada kelas
            tertentu dan mengandung kata dengan id term (A,B,C,D) untuk label positif
            dan (B,A,D,C) apabila label negatif
            selanjutnya dilakukan perhitungan nilai chi square dengan memanggil
            kelas untung menghitung chi square dengan passing variable A,B,C,D
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        PreparedStatement ps = null;

        ArrayList<Integer> ID = new ArrayList<>(getID("Term"));

        try {

            // initiate
            String query = "insert into Chi values (NULL, ?, ?, ?, ?, ?, ?, ?)";
            ps = connect.prepareStatement(query);

            for (Integer id : ID) {

                int count = getCount("Review", null);
                int AB = getCS_AB(id);
                int BA = getCS_BA(id);
                int CD = getCS_CD(id);
                int DC = getCS_DC(id);

                ChiSquare CS = new ChiSquare(count, AB, BA, CD, DC);

                Double chi_positive = BigDecimal.valueOf(CS.getCS_Positive()).setScale(5, RoundingMode.HALF_UP).doubleValue();
                Double chi_negative = BigDecimal.valueOf(CS.getCS_Negative()).setScale(5, RoundingMode.HALF_UP).doubleValue();
                Double chi_square = BigDecimal.valueOf(CS.getCS()).setScale(5, RoundingMode.HALF_UP).doubleValue();

                ps.setInt(1, AB);
                ps.setInt(2, BA);
                ps.setInt(3, CD);
                ps.setInt(4, DC);
                ps.setDouble(5, chi_positive);
                ps.setDouble(6, chi_negative);
                ps.setDouble(7, chi_square);

                ps.addBatch();
            }

            // execute batch queries
            ps.executeBatch();

            // update term table
            updateTerm();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }

    // update term table LINKING TERM WITH CHI SQUARE
    private void updateTerm() {

        /*
            update tabel term setelah dilakukan operasi insert / perhitungan
            nilai chi square dengan menambahkan nilai pada kolom chi pada tabel
            term yang menandakan term dengan id A memiliki nilai chi dengan
            id A pada tabel chi square
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        // get chi square and term id
        ArrayList<Integer> CSID = new ArrayList<>(getID("Chi"));
        ArrayList<Integer> termID = new ArrayList<>(getID("Term"));
        int counter = 0;

        try {

            state = connect.createStatement();
            for (Integer temp : CSID) {

                // initiate query
                String query = "update Term set ID_Chi = " + temp
                        + " where Term.ID = " + termID.get(counter);

                state.addBatch(query);

                counter++;
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    // insert and generate model table
    private void insertModel() {

        /*
            melakukan operasi insert tabel model dengan masing masing
            prosentase
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        PreparedStatement ps = null;

        try {

            // initiate
            String query = "insert into Model values (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            ps = connect.prepareStatement(query);

            Set set = termPercentage.entrySet();
            Iterator iterator = set.iterator();

            int counter = 0;

            while (iterator.hasNext()) {

                Map.Entry entry = (Map.Entry) iterator.next();

                int percentage = (int) entry.getKey();
                int posDocs = getCount("Review", "1");
                int negDocs = getCount("Review", "0");
                Double posPrior = BigDecimal.valueOf(Double.valueOf(posDocs) / getCount("Review", null)).setScale(5, RoundingMode.HALF_UP).doubleValue();
                Double negPrior = BigDecimal.valueOf(Double.valueOf(negDocs) / getCount("Review", null)).setScale(5, RoundingMode.HALF_UP).doubleValue();
                Double posLog = BigDecimal.valueOf(Math.log10(posPrior)).setScale(5, RoundingMode.HALF_UP).doubleValue();
                Double negLog = BigDecimal.valueOf(Math.log10(negPrior)).setScale(5, RoundingMode.HALF_UP).doubleValue();
                int posFreq = getModelTermFreq(limitCount.get(counter), 1, 0);
                int negFreq = getModelTermFreq(limitCount.get(counter), 0, 0);
                int unique = limitCount.get(counter);

                ps.setInt(1, percentage);
                ps.setInt(2, posDocs);
                ps.setInt(3, negDocs);
                ps.setDouble(4, posPrior);
                ps.setDouble(5, negPrior);
                ps.setDouble(6, posLog);
                ps.setDouble(7, negLog);
                ps.setInt(8, posFreq);
                ps.setInt(9, negFreq);
                ps.setInt(10, unique);

                ps.addBatch();

                counter++;
            }

            // execute batch queries
            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }

    // insert and generate model table LINKING TERM WITH MODEL
    private void insertTermProbModel() {

        /*
            mengisi tabel penghubung antara tabel term dan model
            contoh : model A memiliki kata 1, model A memiliki kata 2
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        try {

            // initiate
            state = connect.createStatement();

            Set set = termPercentage.entrySet();
            Iterator iterator = set.iterator();

            int counter = 0;

            while (iterator.hasNext()) {

                Map.Entry entry = (Map.Entry) iterator.next();

                ArrayList<Integer> ID = (ArrayList<Integer>) entry.getValue();

                for (Integer id : ID) {

                    String query = "insert into Term_Probability_Model (ID, ID_Term, ID_Model)\n"
                            + "values (NULL, " + id + ", (select ID from Model where Model.Percent = " + entry.getKey() + "))";

                    state.addBatch(query);
                }

                counter++;
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    // insert and calculate probability TRAINING CLASSIFIER
    private void insertProbability() {

        /*
            melakukan kalkulasi probabilitas pada tiap kata pada tiap prosentase chi square
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        PreparedStatement ps = null;

        try {

            // initiate
            String query = "insert into Probability values (NULL, ?, ?, ?, ?, ?, ?)";
            ps = connect.prepareStatement(query);

            Set set = termPercentage.entrySet();
            Iterator iterator = set.iterator();

            int counter = 0;

            while (iterator.hasNext()) {

                Map.Entry entry = (Map.Entry) iterator.next();

                ArrayList<Integer> term = (ArrayList<Integer>) entry.getValue();

                for (Integer id : term) {

                    int pos_freq = getModelTermFreq(limitCount.get(counter), 1, id);
                    int neg_freq = getModelTermFreq(limitCount.get(counter), 0, id);
                    int pos_MF = getModelLabelFreq(1, (Integer) entry.getKey());
                    int neg_MF = getModelLabelFreq(0, (Integer) entry.getKey());
                    int unique = getModelLabelFreq(2, (Integer) entry.getKey());

                    Double pos_Poster = BigDecimal.valueOf((Double.valueOf(pos_freq) + 1.0) / (Double.valueOf(pos_MF) + Double.valueOf(unique))).setScale(5, RoundingMode.HALF_UP).doubleValue();
                    Double neg_Poster = BigDecimal.valueOf((Double.valueOf(neg_freq) + 1.0) / (Double.valueOf(neg_MF) + Double.valueOf(unique))).setScale(5, RoundingMode.HALF_UP).doubleValue();

                    Double pos_Log = BigDecimal.valueOf(Double.valueOf(Math.log10(pos_Poster))).setScale(5, RoundingMode.HALF_UP).doubleValue();
                    Double neg_Log = BigDecimal.valueOf(Double.valueOf(Math.log10(neg_Poster))).setScale(5, RoundingMode.HALF_UP).doubleValue();

                    ps.setInt(1, pos_freq);
                    ps.setInt(2, neg_freq);
                    ps.setDouble(3, pos_Poster);
                    ps.setDouble(4, neg_Poster);
                    ps.setDouble(5, pos_Log);
                    ps.setDouble(6, neg_Log);

                    ps.addBatch();
                }

                counter++;
            }

            // execute batch queries
            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }

    // update TermProbModel LINKING TERM AND MODEL WITH PROBABILITY
    private void updateTermProbModel() {

        /*
            mengisi tabel penghubung antara kata dan probabilitas
            contoh : model A memiliki kata B dengan probabilitas C
                     model B memiliki kata B dengan probabilitas D
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        try {

            // initiate
            state = connect.createStatement();

            ArrayList<Integer> ID = getID("Probability");

            for (Integer id : ID) {

                String query = "update Term_Probability_Model as TPM set ID_Probability = " + id
                        + " where TPM.ID = " + id;

                state.addBatch(query);
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

    }

    // get all id from table
    private ArrayList<Integer> getID(String param) {

        /*
            query pembantu untuk mendapatkan semua ID dari tabel tertentu
            parameter merupakan nama tabel yang akan diambil semua ID nya
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;
        ResultSet set = null;

        ArrayList<Integer> ID = new ArrayList<>();

        try {

            // initiate
            String query = "select ID from " + param;
            state = connect.createStatement();
            set = state.executeQuery(query);

            while (set.next()) {

                ID.add(set.getInt("ID"));
            }

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return ID;
    }

    // get data of table
    public ArrayList<Integer> getCSTermID(int param) {

        /*
            query pembantu dalam operasi insert MODEL
            mendapatkan id kata mana saja yang akan dimasukkan kedalam
            model dengan presentase 100% model B 75% model C 50%
            dengan parameter batas / jumlah kata (bukan presentase)
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;
        ResultSet set = null;

        ArrayList<Integer> ID = new ArrayList<>();

        try {

            // initiate
            String query = "select Term.ID as ID from Term\n"
                    + "inner join Chi on Term.ID_Chi = Chi.ID\n"
                    + "order by Chi.Score DESC\n"
                    + "limit " + param;

            state = connect.createStatement();
            set = state.executeQuery(query);

            while (set.next()) {

                ID.add(set.getInt("ID"));
            }

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return ID;
    }

    // get total review or document
    public int getCount(String param1, String param2) {

        /*
            query pembantu untuk menghitung jumlah baris pada tabel SQL
            parameter merupakan nama tabel dengan huruf kapital
            sebagai contoh, method ini digunakan pada perhitungan chi square
            untuk mendapatkan jumlah total review
            parameter 1 : nama table
            parameter 2 : label dokumen
            jika parameter 2 null, total seluruh dok positif dan negatif
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int count = 0;

        try {

            // initiate
            String query = "select count(" + param1 + ") as Total from " + param1;

            if (param2 != null) {

                query = query + " where Label = " + param2;
            }

            state = connect.createStatement();
            count = state.executeQuery(query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return count;
    }

    // param 1 and 2 : get total positive frequency and negative frequency for model
    // param 3 : get total frequency of selected word in selected label in selected model percentage
    private int getModelTermFreq(int param1, int param2, int param3) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int count = 0;

        try {

            // initiate
            String query = "select sum(RT.Total) as Total from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n";

            if (param3 != 0) {

                query = query + "inner join Term as T on RT.ID_Term = T.ID\n";
            }

            query = query + "where RT.ID_Term in (\n"
                    + "select Term.ID as ID from Term\n"
                    + "inner join Chi on Term.ID_Chi = Chi.ID\n"
                    + "order by Chi.Score DESC\n"
                    + "limit " + param1 + ")\n"
                    + "and R.Label = " + param2 + "\n";

            if (param3 != 0) {

                query = query + "and T.ID = " + param3;
            }

            state = connect.createStatement();
            count = state.executeQuery(query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return count;
    }

    // get total term frequency of a label stored in model (100, 75, 50)
    // if all parameter 1 is 2, get total unique term stored in model
    private int getModelLabelFreq(int param1, int param2) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int count = 0;

        String selector = null;

        try {

            switch (param1) {

                case 0:
                    selector = "Freq_Negative";
                    break;
                case 1:
                    selector = "Freq_Positive";
                    break;
                case 2:
                    selector = "Term_Total";
                    break;
            }
            // initiate
            String query = "select " + selector + " as Total from Model where Percent = " + param2;

            state = connect.createStatement();
            count = state.executeQuery(query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return count;
    }

    // get total document in a class that contain term
    private int getCS_AB(int param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int AB = 0;

        try {

            String query = "select ifnull(\n"
                    + "(select count(RT.ID) as Total from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n"
                    + "inner join Term as T on RT.ID_Term = T.ID\n"
                    + "where T.ID = " + param + "\n"
                    + "and R.Label = 1\n"
                    + "), 0) as Total";

            state = connect.createStatement();
            AB = state.executeQuery(query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return AB;
    }

    // get total document not in a class that contain term
    private int getCS_BA(int param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int BA = 0;

        try {

            String query = "select ifnull(\n"
                    + "(select count(RT.ID) as Total from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n"
                    + "inner join Term as T on RT.ID_Term = T.ID\n"
                    + "where T.ID = " + param + "\n"
                    + "and R.Label = 0\n"
                    + "), 0) as Total";

            state = connect.createStatement();
            BA = state.executeQuery(query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return BA;
    }

    // get total document in a class that doesn't contain term
    private int getCS_CD(int param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int CD = 0;

        try {

            String query = "select ifnull(\n"
                    + "(select count(distinct R.ID) as Total from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n"
                    + "inner join Term as T on RT.ID_Term = T.ID\n"
                    + "where R.ID not in\n"
                    + "(select R.ID from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n"
                    + "inner join Term as T on RT.ID_Term = T.ID\n"
                    + "where T.ID = " + param + ")\n"
                    + "and R.Label = 1), 0) as Total";

            state = connect.createStatement();
            CD = state.executeQuery(query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return CD;
    }

    // get total document not in a class that doesn't contain term
    private int getCS_DC(int param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        int DC = 0;

        try {

            String Query = "select ifnull(\n"
                    + "(select count(distinct R.ID) as Total from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n"
                    + "inner join Term as T on RT.ID_Term = T.ID\n"
                    + "where R.ID not in\n"
                    + "(select R.ID from Review_Term as RT\n"
                    + "inner join Review as R on RT.ID_Review = R.ID\n"
                    + "inner join Term as T on RT.ID_Term = T.ID\n"
                    + "where T.ID = " + param + ")\n"
                    + "and R.Label = 0), 0) as Total";

            state = connect.createStatement();
            DC = state.executeQuery(Query).getInt("Total");

        } catch (SQLException ex) {

            Logger.getLogger(TrainQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return DC;
    }
}
