package PreProcess;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.index.IndexWriterConfig;

public class Preprocessing {

    private Analyzer analyzer;
    private IndexWriterConfig config;

    public Preprocessing() {

        /*
            preprocessing teks menggunakan lucene analyzer
            inisiasi dilakukan dengan memasukkan / passing
            parameter berupa file stopword, selanjutnya method anaylze
            di panggil untuk memproses tiap dokumen pada arraylist class
         */
        Reader read;

        try {

            File file = new File("./data/stopword.txt");
            read = new FileReader(file);
            analyzer = new StandardAnalyzer(read);

            read.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Preprocessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Preprocessing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String> analyze(String text) {

        ArrayList<String> result = new ArrayList<>();

        // proses teks preprocessing menggunakan library lucene
        TokenStream stream = analyzer.tokenStream(null, new StringReader(text));
        stream = new PorterStemFilter(stream);

        try {

            stream.reset();

            // variable pembantu untuk menentukan kata negasi atau tidak
            String temp = "";

            while (stream.incrementToken()) {

                // melakukan pengecekan apakah variable temp = kosong dan
                // apabila tidak dilakukan penambahan kata negasi NOT_
                if (!temp.isEmpty()) {

                    result.add(temp + stream.getAttribute(CharTermAttribute.class).toString());

                    temp = "";
                }

                // melakukan pengecekan apakah kata termasuk negasi atau tidak
                // dengan melakukan pengecekan apakah kata merupakan
                // kata no atau not, jika ya, set variabel temp dengan kata NOT_
                // jika tidak masukkan kata pada hasil akhir
                if (stream.getAttribute(CharTermAttribute.class).toString().equals("no") || stream.getAttribute(CharTermAttribute.class).toString().equals("not")) {

                    temp = "NOT_";

                } else {

                    result.add(stream.getAttribute(CharTermAttribute.class).toString());
                }
            }

            stream.close();

        } catch (IOException e) {

        }

        return result;
    }
}
