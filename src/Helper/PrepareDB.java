/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isalindrawan
 */
public class PrepareDB {

    public void deleteExistingUserDB() {

        File delete = new File("./temp/user_model.db");
        File create = new File("./data/model.db.orig");

        if (delete.exists()) {

            delete.delete();

        }

        try {

            Files.copy(create.toPath(), delete.toPath(), StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException ex) {
            Logger.getLogger(PrepareDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteExistingResultDB() {

        File delete = new File("./temp/sentiment_model.db");
        File create = new File("./data/sentiment.db.orig");

        if (delete.exists()) {

            delete.delete();

        }

        try {

            Files.copy(create.toPath(), delete.toPath(), StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException ex) {
            Logger.getLogger(PrepareDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String> duplicateDB(int count) {

        reset();

        ArrayList<String> db_name = new ArrayList<>();

        for (int i = 1; i <= count; i++) {

            File delete = new File("./temp/sentiment_model_" + i + ".db");
            File create = new File("./data/sentiment.db.orig");

            if (delete.exists()) {

                delete.delete();

            }

            try {

                Files.copy(create.toPath(), delete.toPath(), StandardCopyOption.REPLACE_EXISTING);
                db_name.add("./temp/" + delete.getName());

            } catch (IOException ex) {
                Logger.getLogger(PrepareDB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return db_name;
    }

    public void reset() {

        int count = 1;
        File delete = new File("./temp/sentiment_model_" + count + ".db");

        while (delete.exists()) {

            delete.delete();

            count++;

            delete = new File("./temp/sentiment_model_" + count + ".db");
        }
    }
}
