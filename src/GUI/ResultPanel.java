/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Helper.AdditionalQueries;
import java.text.DecimalFormat;

/**
 *
 * @author isalindrawan
 */
public class ResultPanel extends javax.swing.JPanel {

    private String training_db;
    private boolean preset;

    /**
     * Creates new form resultPanel
     *
     * @param preset
     * @param training_db
     */
    public ResultPanel(boolean preset, String training_db) {

        this.training_db = training_db;
        this.preset = preset;

        initComponents();
        generateValue();

    }

    public javax.swing.JPanel getPanel() {

        return this;
    }

    private void generateValue() {

        DecimalFormat numberFormat = new DecimalFormat("#.00");

        trainAppField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Application", false, null, null)));
        trainDocField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Review", false, null, null)));
        trainPositiveField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Review", true, "Review.Label", "1")));
        trainNegativeField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Review", true, "Review.Label", "0")));
        trainModelField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Model", false, null, null)));
        trainTermField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount(training_db, "Term", false, null, null)));

        testAppField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount("./temp/sentiment_model.db", "Application", false, null, null)));
        testDocField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount("./temp/sentiment_model.db", "Review", false, null, null)));
        testPositiveField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount("./temp/sentiment_model.db", "Review", true, "Review.Label", "1")));
        testNegativeField.setText(" : " + String.valueOf(new AdditionalQueries().getRowCount("./temp/sentiment_model.db", "Review", true, "Review.Label", "0")));
        testPresetField.setText(" : " + String.valueOf(preset));

        posPredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 1, 1)));
        negPredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 1, 0)));
        truePredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 1, true)));
        falsePredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 1, false)));
        accuracyPredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Accuracy", 1) + " %"));
        precisionPredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Precision", 1) + " %"));
        recallPredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Recall", 1) + " %"));
        measurePredictField1.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Measure", 1) + " %"));
        timeField1.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Time", 1) + " ms"));

        posPredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 2, 1)));
        negPredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 2, 0)));
        truePredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 2, true)));
        falsePredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 2, false)));
        accuracyPredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Accuracy", 2) + " %"));
        precisionPredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Precision", 2) + " %"));
        recallPredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Recall", 2) + " %"));
        measurePredictField2.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Measure", 2) + " %"));
        timeField2.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Time", 2) + " ms"));

        posPredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 3, 1)));
        negPredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 3, 0)));
        truePredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 3, true)));
        falsePredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 3, false)));
        accuracyPredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Accuracy", 3) + " %"));
        precisionPredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Precision", 3) + " %"));
        recallPredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Recall", 3) + " %"));
        measurePredictField3.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Measure", 3) + " %"));
        timeField3.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Time", 3) + " ms"));

        posPredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 4, 1)));
        negPredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getCountPredictionLabel("./temp/sentiment_model.db", 4, 0)));
        truePredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 4, true)));
        falsePredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getCountPrediction("./temp/sentiment_model.db", 4, false)));
        accuracyPredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Accuracy", 4) + " %"));
        precisionPredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Precision", 4) + " %"));
        recallPredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Recall", 4) + " %"));
        measurePredictField4.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Measure", 4) + " %"));
        timeField4.setText(" : " + String.valueOf(new AdditionalQueries().getAccuracy("./temp/sentiment_model.db", "Time", 4) + " ms"));

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topPanel = new javax.swing.JPanel();
        trainPanel = new javax.swing.JPanel();
        trainContainer = new javax.swing.JPanel();
        trainAppLabel = new javax.swing.JLabel();
        trainAppField = new javax.swing.JLabel();
        trainDocLabel = new javax.swing.JLabel();
        trainDocField = new javax.swing.JLabel();
        trainPositiveLabel = new javax.swing.JLabel();
        trainPositiveField = new javax.swing.JLabel();
        trainNegativeLabel = new javax.swing.JLabel();
        trainNegativeField = new javax.swing.JLabel();
        trainModelLabel = new javax.swing.JLabel();
        trainModelField = new javax.swing.JLabel();
        trainTermLabel = new javax.swing.JLabel();
        trainTermField = new javax.swing.JLabel();
        testPanel = new javax.swing.JPanel();
        testContainer = new javax.swing.JPanel();
        testAppLabel = new javax.swing.JLabel();
        testAppField = new javax.swing.JLabel();
        testDocLabel = new javax.swing.JLabel();
        testDocField = new javax.swing.JLabel();
        testPositiveLabel = new javax.swing.JLabel();
        testPositiveField = new javax.swing.JLabel();
        testNegativeLabel = new javax.swing.JLabel();
        testNegativeField = new javax.swing.JLabel();
        testPresetLabel = new javax.swing.JLabel();
        testPresetField = new javax.swing.JLabel();
        bottomPanel = new javax.swing.JPanel();
        CS100Panel = new javax.swing.JPanel();
        CS100Container = new javax.swing.JPanel();
        posPredictLabel4 = new javax.swing.JLabel();
        posPredictField4 = new javax.swing.JLabel();
        negPredictLabel4 = new javax.swing.JLabel();
        negPredictField4 = new javax.swing.JLabel();
        truePredictLabel4 = new javax.swing.JLabel();
        truePredictField4 = new javax.swing.JLabel();
        falsePredictLabel4 = new javax.swing.JLabel();
        falsePredictField4 = new javax.swing.JLabel();
        accuracyLabel4 = new javax.swing.JLabel();
        accuracyPredictField4 = new javax.swing.JLabel();
        precisionLabel4 = new javax.swing.JLabel();
        precisionPredictField4 = new javax.swing.JLabel();
        recallLabel4 = new javax.swing.JLabel();
        recallPredictField4 = new javax.swing.JLabel();
        measureLabel4 = new javax.swing.JLabel();
        measurePredictField4 = new javax.swing.JLabel();
        timeLabel4 = new javax.swing.JLabel();
        timeField4 = new javax.swing.JLabel();
        CS25Panel = new javax.swing.JPanel();
        CS25Container = new javax.swing.JPanel();
        posPredictLabel1 = new javax.swing.JLabel();
        posPredictField1 = new javax.swing.JLabel();
        negPredictLabel1 = new javax.swing.JLabel();
        negPredictField1 = new javax.swing.JLabel();
        truePredictLabel1 = new javax.swing.JLabel();
        truePredictField1 = new javax.swing.JLabel();
        falsePredictLabel1 = new javax.swing.JLabel();
        falsePredictField1 = new javax.swing.JLabel();
        accuracyLabel1 = new javax.swing.JLabel();
        accuracyPredictField1 = new javax.swing.JLabel();
        precisionLabel1 = new javax.swing.JLabel();
        precisionPredictField1 = new javax.swing.JLabel();
        recallLabel1 = new javax.swing.JLabel();
        recallPredictField1 = new javax.swing.JLabel();
        measureLabel1 = new javax.swing.JLabel();
        measurePredictField1 = new javax.swing.JLabel();
        timeLabel1 = new javax.swing.JLabel();
        timeField1 = new javax.swing.JLabel();
        CS50Panel = new javax.swing.JPanel();
        CS50Container = new javax.swing.JPanel();
        posPredictLabel2 = new javax.swing.JLabel();
        posPredictField2 = new javax.swing.JLabel();
        negPredictLabel2 = new javax.swing.JLabel();
        negPredictField2 = new javax.swing.JLabel();
        truePredictLabel2 = new javax.swing.JLabel();
        truePredictField2 = new javax.swing.JLabel();
        falsePredictLabel2 = new javax.swing.JLabel();
        falsePredictField2 = new javax.swing.JLabel();
        accuracyLabel2 = new javax.swing.JLabel();
        accuracyPredictField2 = new javax.swing.JLabel();
        precisionLabel2 = new javax.swing.JLabel();
        precisionPredictField2 = new javax.swing.JLabel();
        recallLabel2 = new javax.swing.JLabel();
        recallPredictField2 = new javax.swing.JLabel();
        measureLabel2 = new javax.swing.JLabel();
        measurePredictField2 = new javax.swing.JLabel();
        timeLabel2 = new javax.swing.JLabel();
        timeField2 = new javax.swing.JLabel();
        CS75Panel = new javax.swing.JPanel();
        CS75Container = new javax.swing.JPanel();
        posPredictLabel3 = new javax.swing.JLabel();
        posPredictField3 = new javax.swing.JLabel();
        negPredictLabel3 = new javax.swing.JLabel();
        negPredictField3 = new javax.swing.JLabel();
        truePredictLabel3 = new javax.swing.JLabel();
        truePredictField3 = new javax.swing.JLabel();
        falsePredictLabel3 = new javax.swing.JLabel();
        falsePredictField3 = new javax.swing.JLabel();
        accuracyLabel3 = new javax.swing.JLabel();
        accuracyPredictField3 = new javax.swing.JLabel();
        precisionLabel3 = new javax.swing.JLabel();
        precisionPredictField3 = new javax.swing.JLabel();
        recallLabel3 = new javax.swing.JLabel();
        recallPredictField3 = new javax.swing.JLabel();
        measureLabel3 = new javax.swing.JLabel();
        measurePredictField3 = new javax.swing.JLabel();
        timeLabel3 = new javax.swing.JLabel();
        timeField3 = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(734, 493));
        setPreferredSize(new java.awt.Dimension(734, 493));

        topPanel.setMinimumSize(new java.awt.Dimension(714, 160));
        topPanel.setPreferredSize(new java.awt.Dimension(714, 160));
        topPanel.setLayout(new java.awt.GridLayout(1, 0));

        trainPanel.setBackground(new java.awt.Color(255, 255, 255));
        trainPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATA LATIH", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        trainContainer.setBackground(new java.awt.Color(255, 255, 255));
        trainContainer.setMinimumSize(new java.awt.Dimension(348, 165));
        trainContainer.setLayout(new java.awt.GridLayout(6, 2));

        trainAppLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainAppLabel.setText("Aplikasi");
        trainAppLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainAppLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainAppLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        trainContainer.add(trainAppLabel);

        trainAppField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainAppField.setText(" : ");
        trainAppField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainAppField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainAppField.setPreferredSize(new java.awt.Dimension(206, 15));
        trainContainer.add(trainAppField);

        trainDocLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainDocLabel.setText("Dokumen");
        trainDocLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainDocLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainDocLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        trainContainer.add(trainDocLabel);

        trainDocField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainDocField.setText(" : ");
        trainDocField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainDocField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainDocField.setPreferredSize(new java.awt.Dimension(206, 15));
        trainContainer.add(trainDocField);

        trainPositiveLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainPositiveLabel.setText("Positif");
        trainPositiveLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainPositiveLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainPositiveLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        trainContainer.add(trainPositiveLabel);

        trainPositiveField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainPositiveField.setText(" : ");
        trainPositiveField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainPositiveField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainPositiveField.setPreferredSize(new java.awt.Dimension(206, 15));
        trainContainer.add(trainPositiveField);

        trainNegativeLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainNegativeLabel.setText("Negatif");
        trainNegativeLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainNegativeLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainNegativeLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        trainContainer.add(trainNegativeLabel);

        trainNegativeField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainNegativeField.setText(" : ");
        trainNegativeField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainNegativeField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainNegativeField.setPreferredSize(new java.awt.Dimension(206, 15));
        trainContainer.add(trainNegativeField);

        trainModelLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainModelLabel.setText("Model");
        trainModelLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainModelLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainModelLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        trainContainer.add(trainModelLabel);

        trainModelField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainModelField.setText(" : ");
        trainModelField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainModelField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainModelField.setPreferredSize(new java.awt.Dimension(206, 15));
        trainContainer.add(trainModelField);

        trainTermLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainTermLabel.setText("Term");
        trainTermLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        trainTermLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        trainTermLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        trainContainer.add(trainTermLabel);

        trainTermField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        trainTermField.setText(" : ");
        trainTermField.setMaximumSize(new java.awt.Dimension(206, 15));
        trainTermField.setMinimumSize(new java.awt.Dimension(206, 15));
        trainTermField.setPreferredSize(new java.awt.Dimension(206, 15));
        trainContainer.add(trainTermField);

        javax.swing.GroupLayout trainPanelLayout = new javax.swing.GroupLayout(trainPanel);
        trainPanel.setLayout(trainPanelLayout);
        trainPanelLayout.setHorizontalGroup(
            trainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(trainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(trainContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 325, Short.MAX_VALUE)
                .addContainerGap())
        );
        trainPanelLayout.setVerticalGroup(
            trainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, trainPanelLayout.createSequentialGroup()
                .addComponent(trainContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
        );

        topPanel.add(trainPanel);

        testPanel.setBackground(new java.awt.Color(255, 255, 255));
        testPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATA UJI", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        testContainer.setBackground(new java.awt.Color(255, 255, 255));
        testContainer.setMinimumSize(new java.awt.Dimension(348, 165));
        testContainer.setPreferredSize(new java.awt.Dimension(348, 165));
        testContainer.setLayout(new java.awt.GridLayout(6, 2));

        testAppLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppLabel.setText("Aplikasi");
        testAppLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        testAppLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        testAppLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        testContainer.add(testAppLabel);

        testAppField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testAppField.setText(" : ");
        testAppField.setMaximumSize(new java.awt.Dimension(206, 15));
        testAppField.setMinimumSize(new java.awt.Dimension(206, 15));
        testAppField.setPreferredSize(new java.awt.Dimension(206, 15));
        testContainer.add(testAppField);

        testDocLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocLabel.setText("Dokumen");
        testDocLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        testDocLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        testDocLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        testContainer.add(testDocLabel);

        testDocField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testDocField.setText(" : ");
        testDocField.setMaximumSize(new java.awt.Dimension(206, 15));
        testDocField.setMinimumSize(new java.awt.Dimension(206, 15));
        testDocField.setPreferredSize(new java.awt.Dimension(206, 15));
        testContainer.add(testDocField);

        testPositiveLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveLabel.setText("Positif");
        testPositiveLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        testPositiveLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        testContainer.add(testPositiveLabel);

        testPositiveField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPositiveField.setText(" : ");
        testPositiveField.setMaximumSize(new java.awt.Dimension(206, 15));
        testPositiveField.setMinimumSize(new java.awt.Dimension(206, 15));
        testPositiveField.setPreferredSize(new java.awt.Dimension(206, 15));
        testContainer.add(testPositiveField);

        testNegativeLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeLabel.setText("Negatif");
        testNegativeLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        testNegativeLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        testContainer.add(testNegativeLabel);

        testNegativeField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testNegativeField.setText(" : ");
        testNegativeField.setMaximumSize(new java.awt.Dimension(206, 15));
        testNegativeField.setMinimumSize(new java.awt.Dimension(206, 15));
        testNegativeField.setPreferredSize(new java.awt.Dimension(206, 15));
        testContainer.add(testNegativeField);

        testPresetLabel.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPresetLabel.setText("Preset");
        testPresetLabel.setMaximumSize(new java.awt.Dimension(100, 15));
        testPresetLabel.setMinimumSize(new java.awt.Dimension(100, 15));
        testPresetLabel.setPreferredSize(new java.awt.Dimension(100, 15));
        testContainer.add(testPresetLabel);

        testPresetField.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        testPresetField.setText(" : ");
        testPresetField.setMaximumSize(new java.awt.Dimension(206, 15));
        testPresetField.setMinimumSize(new java.awt.Dimension(206, 15));
        testPresetField.setPreferredSize(new java.awt.Dimension(206, 15));
        testContainer.add(testPresetField);

        javax.swing.GroupLayout testPanelLayout = new javax.swing.GroupLayout(testPanel);
        testPanel.setLayout(testPanelLayout);
        testPanelLayout.setHorizontalGroup(
            testPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(testContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 325, Short.MAX_VALUE)
                .addContainerGap())
        );
        testPanelLayout.setVerticalGroup(
            testPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(testPanelLayout.createSequentialGroup()
                .addComponent(testContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 125, Short.MAX_VALUE)
                .addContainerGap())
        );

        topPanel.add(testPanel);

        bottomPanel.setMinimumSize(new java.awt.Dimension(714, 300));
        bottomPanel.setPreferredSize(new java.awt.Dimension(714, 300));
        bottomPanel.setLayout(new java.awt.GridLayout(1, 0));

        CS100Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS100Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TANPA SELEKSI FITUR", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        CS100Container.setBackground(new java.awt.Color(255, 255, 255));
        CS100Container.setLayout(new java.awt.GridLayout(10, 2));

        posPredictLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictLabel4.setText("Positif");
        posPredictLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        posPredictLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        posPredictLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(posPredictLabel4);

        posPredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictField4.setText(" : ");
        posPredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        posPredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        posPredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(posPredictField4);

        negPredictLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictLabel4.setText("Negatif");
        negPredictLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        negPredictLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        negPredictLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(negPredictLabel4);

        negPredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictField4.setText(" : ");
        negPredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        negPredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        negPredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(negPredictField4);

        truePredictLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictLabel4.setText("Benar");
        truePredictLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        truePredictLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        truePredictLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(truePredictLabel4);

        truePredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictField4.setText(" : ");
        truePredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        truePredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        truePredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(truePredictField4);

        falsePredictLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictLabel4.setText("Salah");
        falsePredictLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(falsePredictLabel4);

        falsePredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictField4.setText(" : ");
        falsePredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        falsePredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        falsePredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(falsePredictField4);

        accuracyLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyLabel4.setText("Akurasi");
        accuracyLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        accuracyLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        accuracyLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(accuracyLabel4);

        accuracyPredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyPredictField4.setText(" : ");
        accuracyPredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(accuracyPredictField4);

        precisionLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionLabel4.setText("Presisi");
        precisionLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        precisionLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        precisionLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(precisionLabel4);

        precisionPredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionPredictField4.setText(" : ");
        precisionPredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        precisionPredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        precisionPredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(precisionPredictField4);

        recallLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallLabel4.setText("Recall");
        recallLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        recallLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        recallLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(recallLabel4);

        recallPredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallPredictField4.setText(" : ");
        recallPredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        recallPredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        recallPredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(recallPredictField4);

        measureLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measureLabel4.setText("F-Measure");
        measureLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        measureLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        measureLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(measureLabel4);

        measurePredictField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measurePredictField4.setText(" : ");
        measurePredictField4.setMaximumSize(new java.awt.Dimension(84, 15));
        measurePredictField4.setMinimumSize(new java.awt.Dimension(84, 15));
        measurePredictField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(measurePredictField4);

        timeLabel4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeLabel4.setText("Time");
        timeLabel4.setMaximumSize(new java.awt.Dimension(100, 15));
        timeLabel4.setMinimumSize(new java.awt.Dimension(100, 15));
        timeLabel4.setPreferredSize(new java.awt.Dimension(100, 15));
        CS100Container.add(timeLabel4);

        timeField4.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeField4.setText(" : ");
        timeField4.setMaximumSize(new java.awt.Dimension(84, 15));
        timeField4.setMinimumSize(new java.awt.Dimension(84, 15));
        timeField4.setPreferredSize(new java.awt.Dimension(84, 15));
        CS100Container.add(timeField4);

        javax.swing.GroupLayout CS100PanelLayout = new javax.swing.GroupLayout(CS100Panel);
        CS100Panel.setLayout(CS100PanelLayout);
        CS100PanelLayout.setHorizontalGroup(
            CS100PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS100PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS100Container, javax.swing.GroupLayout.PREFERRED_SIZE, 146, Short.MAX_VALUE)
                .addContainerGap())
        );
        CS100PanelLayout.setVerticalGroup(
            CS100PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS100PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS100Container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        bottomPanel.add(CS100Panel);

        CS25Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS25Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "25% SELEKSI FITUR ", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        CS25Container.setBackground(new java.awt.Color(255, 255, 255));
        CS25Container.setMinimumSize(new java.awt.Dimension(225, 230));
        CS25Container.setPreferredSize(new java.awt.Dimension(225, 230));
        CS25Container.setLayout(new java.awt.GridLayout(10, 2));

        posPredictLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictLabel1.setText("Positif");
        posPredictLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        posPredictLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(posPredictLabel1);

        posPredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictField1.setText(" : ");
        posPredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        posPredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        posPredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(posPredictField1);

        negPredictLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictLabel1.setText("Negatif");
        negPredictLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        negPredictLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(negPredictLabel1);

        negPredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictField1.setText(" : ");
        negPredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        negPredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        negPredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(negPredictField1);

        truePredictLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictLabel1.setText("Benar");
        truePredictLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        truePredictLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(truePredictLabel1);

        truePredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictField1.setText(" : ");
        truePredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        truePredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        truePredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(truePredictField1);

        falsePredictLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictLabel1.setText("Salah");
        falsePredictLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(falsePredictLabel1);

        falsePredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictField1.setText(" : ");
        falsePredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        falsePredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        falsePredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(falsePredictField1);

        accuracyLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyLabel1.setText("Akurasi");
        accuracyLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        accuracyLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(accuracyLabel1);

        accuracyPredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyPredictField1.setText(" : ");
        accuracyPredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(accuracyPredictField1);

        precisionLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionLabel1.setText("Presisi");
        precisionLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        precisionLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(precisionLabel1);

        precisionPredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionPredictField1.setText(" : ");
        precisionPredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        precisionPredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        precisionPredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(precisionPredictField1);

        recallLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallLabel1.setText("Recall");
        recallLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        recallLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(recallLabel1);

        recallPredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallPredictField1.setText(" : ");
        recallPredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        recallPredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        recallPredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(recallPredictField1);

        measureLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measureLabel1.setText("F-Measure");
        measureLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        measureLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(measureLabel1);

        measurePredictField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measurePredictField1.setText(" : ");
        measurePredictField1.setMaximumSize(new java.awt.Dimension(84, 15));
        measurePredictField1.setMinimumSize(new java.awt.Dimension(84, 15));
        measurePredictField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(measurePredictField1);

        timeLabel1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeLabel1.setText("Time");
        timeLabel1.setMinimumSize(new java.awt.Dimension(100, 15));
        timeLabel1.setPreferredSize(new java.awt.Dimension(100, 15));
        CS25Container.add(timeLabel1);

        timeField1.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeField1.setText(" : ");
        timeField1.setMaximumSize(new java.awt.Dimension(84, 15));
        timeField1.setMinimumSize(new java.awt.Dimension(84, 15));
        timeField1.setPreferredSize(new java.awt.Dimension(84, 15));
        CS25Container.add(timeField1);

        javax.swing.GroupLayout CS25PanelLayout = new javax.swing.GroupLayout(CS25Panel);
        CS25Panel.setLayout(CS25PanelLayout);
        CS25PanelLayout.setHorizontalGroup(
            CS25PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS25PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS25Container, javax.swing.GroupLayout.PREFERRED_SIZE, 146, Short.MAX_VALUE)
                .addContainerGap())
        );
        CS25PanelLayout.setVerticalGroup(
            CS25PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS25PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS25Container, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                .addContainerGap())
        );

        bottomPanel.add(CS25Panel);

        CS50Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS50Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "50% SELEKSI FITUR ", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        CS50Container.setBackground(new java.awt.Color(255, 255, 255));
        CS50Container.setLayout(new java.awt.GridLayout(10, 2));

        posPredictLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictLabel2.setText("Positif");
        posPredictLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        posPredictLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        posPredictLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(posPredictLabel2);

        posPredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictField2.setText(" : ");
        posPredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        posPredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        posPredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(posPredictField2);

        negPredictLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictLabel2.setText("Negatif");
        negPredictLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        negPredictLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        negPredictLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(negPredictLabel2);

        negPredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictField2.setText(" : ");
        negPredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        negPredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        negPredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(negPredictField2);

        truePredictLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictLabel2.setText("Benar");
        truePredictLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        truePredictLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        truePredictLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(truePredictLabel2);

        truePredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictField2.setText(" : ");
        truePredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        truePredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        truePredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(truePredictField2);

        falsePredictLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictLabel2.setText("Salah");
        falsePredictLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(falsePredictLabel2);

        falsePredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictField2.setText(" : ");
        falsePredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        falsePredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        falsePredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(falsePredictField2);

        accuracyLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyLabel2.setText("Akurasi");
        accuracyLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        accuracyLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        accuracyLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(accuracyLabel2);

        accuracyPredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyPredictField2.setText(" : ");
        accuracyPredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(accuracyPredictField2);

        precisionLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionLabel2.setText("Presisi");
        precisionLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        precisionLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        precisionLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(precisionLabel2);

        precisionPredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionPredictField2.setText(" : ");
        precisionPredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        precisionPredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        precisionPredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(precisionPredictField2);

        recallLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallLabel2.setText("Recall");
        recallLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        recallLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        recallLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(recallLabel2);

        recallPredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallPredictField2.setText(" : ");
        recallPredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        recallPredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        recallPredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(recallPredictField2);

        measureLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measureLabel2.setText("F-Measure");
        measureLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        measureLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        measureLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(measureLabel2);

        measurePredictField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measurePredictField2.setText(" : ");
        measurePredictField2.setMaximumSize(new java.awt.Dimension(84, 15));
        measurePredictField2.setMinimumSize(new java.awt.Dimension(84, 15));
        measurePredictField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(measurePredictField2);

        timeLabel2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeLabel2.setText("Time");
        timeLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        timeLabel2.setMinimumSize(new java.awt.Dimension(100, 15));
        timeLabel2.setPreferredSize(new java.awt.Dimension(100, 15));
        CS50Container.add(timeLabel2);

        timeField2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeField2.setText(" : ");
        timeField2.setMaximumSize(new java.awt.Dimension(84, 15));
        timeField2.setMinimumSize(new java.awt.Dimension(84, 15));
        timeField2.setPreferredSize(new java.awt.Dimension(84, 15));
        CS50Container.add(timeField2);

        javax.swing.GroupLayout CS50PanelLayout = new javax.swing.GroupLayout(CS50Panel);
        CS50Panel.setLayout(CS50PanelLayout);
        CS50PanelLayout.setHorizontalGroup(
            CS50PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS50PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS50Container, javax.swing.GroupLayout.PREFERRED_SIZE, 146, Short.MAX_VALUE)
                .addContainerGap())
        );
        CS50PanelLayout.setVerticalGroup(
            CS50PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS50PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS50Container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        bottomPanel.add(CS50Panel);

        CS75Panel.setBackground(new java.awt.Color(255, 255, 255));
        CS75Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "75% SELEKSI FITUR ", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 12))); // NOI18N

        CS75Container.setBackground(new java.awt.Color(255, 255, 255));
        CS75Container.setLayout(new java.awt.GridLayout(10, 2));

        posPredictLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictLabel3.setText("Positif");
        posPredictLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        posPredictLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        posPredictLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(posPredictLabel3);

        posPredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        posPredictField3.setText(" : ");
        posPredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        posPredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        posPredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(posPredictField3);

        negPredictLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictLabel3.setText("Negatif");
        negPredictLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        negPredictLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        negPredictLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(negPredictLabel3);

        negPredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        negPredictField3.setText(" : ");
        negPredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        negPredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        negPredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(negPredictField3);

        truePredictLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictLabel3.setText("Benar");
        truePredictLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        truePredictLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        truePredictLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(truePredictLabel3);

        truePredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        truePredictField3.setText(" : ");
        truePredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        truePredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        truePredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(truePredictField3);

        falsePredictLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictLabel3.setText("Salah");
        falsePredictLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        falsePredictLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(falsePredictLabel3);

        falsePredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        falsePredictField3.setText(" : ");
        falsePredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        falsePredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        falsePredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(falsePredictField3);

        accuracyLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyLabel3.setText("Akurasi");
        accuracyLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        accuracyLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        accuracyLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(accuracyLabel3);

        accuracyPredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        accuracyPredictField3.setText(" : ");
        accuracyPredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        accuracyPredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(accuracyPredictField3);

        precisionLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionLabel3.setText("Presisi");
        precisionLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        precisionLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        precisionLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(precisionLabel3);

        precisionPredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        precisionPredictField3.setText(" : ");
        precisionPredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        precisionPredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        precisionPredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(precisionPredictField3);

        recallLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallLabel3.setText("Recall");
        recallLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        recallLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        recallLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(recallLabel3);

        recallPredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        recallPredictField3.setText(" : ");
        recallPredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        recallPredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        recallPredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(recallPredictField3);

        measureLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measureLabel3.setText("F-Measure");
        measureLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        measureLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        measureLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(measureLabel3);

        measurePredictField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        measurePredictField3.setText(" : ");
        measurePredictField3.setMaximumSize(new java.awt.Dimension(84, 15));
        measurePredictField3.setMinimumSize(new java.awt.Dimension(84, 15));
        measurePredictField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(measurePredictField3);

        timeLabel3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeLabel3.setText("Time");
        timeLabel3.setMaximumSize(new java.awt.Dimension(100, 15));
        timeLabel3.setMinimumSize(new java.awt.Dimension(100, 15));
        timeLabel3.setPreferredSize(new java.awt.Dimension(100, 15));
        CS75Container.add(timeLabel3);

        timeField3.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        timeField3.setText(" : ");
        timeField3.setMaximumSize(new java.awt.Dimension(84, 15));
        timeField3.setMinimumSize(new java.awt.Dimension(84, 15));
        timeField3.setPreferredSize(new java.awt.Dimension(84, 15));
        CS75Container.add(timeField3);

        javax.swing.GroupLayout CS75PanelLayout = new javax.swing.GroupLayout(CS75Panel);
        CS75Panel.setLayout(CS75PanelLayout);
        CS75PanelLayout.setHorizontalGroup(
            CS75PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS75PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS75Container, javax.swing.GroupLayout.PREFERRED_SIZE, 146, Short.MAX_VALUE)
                .addContainerGap())
        );
        CS75PanelLayout.setVerticalGroup(
            CS75PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CS75PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CS75Container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        bottomPanel.add(CS75Panel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(bottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(topPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(topPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bottomPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel CS100Container;
    private javax.swing.JPanel CS100Panel;
    private javax.swing.JPanel CS25Container;
    private javax.swing.JPanel CS25Panel;
    private javax.swing.JPanel CS50Container;
    private javax.swing.JPanel CS50Panel;
    private javax.swing.JPanel CS75Container;
    private javax.swing.JPanel CS75Panel;
    private javax.swing.JLabel accuracyLabel1;
    private javax.swing.JLabel accuracyLabel2;
    private javax.swing.JLabel accuracyLabel3;
    private javax.swing.JLabel accuracyLabel4;
    private javax.swing.JLabel accuracyPredictField1;
    private javax.swing.JLabel accuracyPredictField2;
    private javax.swing.JLabel accuracyPredictField3;
    private javax.swing.JLabel accuracyPredictField4;
    private javax.swing.JPanel bottomPanel;
    private javax.swing.JLabel falsePredictField1;
    private javax.swing.JLabel falsePredictField2;
    private javax.swing.JLabel falsePredictField3;
    private javax.swing.JLabel falsePredictField4;
    private javax.swing.JLabel falsePredictLabel1;
    private javax.swing.JLabel falsePredictLabel2;
    private javax.swing.JLabel falsePredictLabel3;
    private javax.swing.JLabel falsePredictLabel4;
    private javax.swing.JLabel measureLabel1;
    private javax.swing.JLabel measureLabel2;
    private javax.swing.JLabel measureLabel3;
    private javax.swing.JLabel measureLabel4;
    private javax.swing.JLabel measurePredictField1;
    private javax.swing.JLabel measurePredictField2;
    private javax.swing.JLabel measurePredictField3;
    private javax.swing.JLabel measurePredictField4;
    private javax.swing.JLabel negPredictField1;
    private javax.swing.JLabel negPredictField2;
    private javax.swing.JLabel negPredictField3;
    private javax.swing.JLabel negPredictField4;
    private javax.swing.JLabel negPredictLabel1;
    private javax.swing.JLabel negPredictLabel2;
    private javax.swing.JLabel negPredictLabel3;
    private javax.swing.JLabel negPredictLabel4;
    private javax.swing.JLabel posPredictField1;
    private javax.swing.JLabel posPredictField2;
    private javax.swing.JLabel posPredictField3;
    private javax.swing.JLabel posPredictField4;
    private javax.swing.JLabel posPredictLabel1;
    private javax.swing.JLabel posPredictLabel2;
    private javax.swing.JLabel posPredictLabel3;
    private javax.swing.JLabel posPredictLabel4;
    private javax.swing.JLabel precisionLabel1;
    private javax.swing.JLabel precisionLabel2;
    private javax.swing.JLabel precisionLabel3;
    private javax.swing.JLabel precisionLabel4;
    private javax.swing.JLabel precisionPredictField1;
    private javax.swing.JLabel precisionPredictField2;
    private javax.swing.JLabel precisionPredictField3;
    private javax.swing.JLabel precisionPredictField4;
    private javax.swing.JLabel recallLabel1;
    private javax.swing.JLabel recallLabel2;
    private javax.swing.JLabel recallLabel3;
    private javax.swing.JLabel recallLabel4;
    private javax.swing.JLabel recallPredictField1;
    private javax.swing.JLabel recallPredictField2;
    private javax.swing.JLabel recallPredictField3;
    private javax.swing.JLabel recallPredictField4;
    private javax.swing.JLabel testAppField;
    private javax.swing.JLabel testAppLabel;
    private javax.swing.JPanel testContainer;
    private javax.swing.JLabel testDocField;
    private javax.swing.JLabel testDocLabel;
    private javax.swing.JLabel testNegativeField;
    private javax.swing.JLabel testNegativeLabel;
    private javax.swing.JPanel testPanel;
    private javax.swing.JLabel testPositiveField;
    private javax.swing.JLabel testPositiveLabel;
    private javax.swing.JLabel testPresetField;
    private javax.swing.JLabel testPresetLabel;
    private javax.swing.JLabel timeField1;
    private javax.swing.JLabel timeField2;
    private javax.swing.JLabel timeField3;
    private javax.swing.JLabel timeField4;
    private javax.swing.JLabel timeLabel1;
    private javax.swing.JLabel timeLabel2;
    private javax.swing.JLabel timeLabel3;
    private javax.swing.JLabel timeLabel4;
    private javax.swing.JPanel topPanel;
    private javax.swing.JLabel trainAppField;
    private javax.swing.JLabel trainAppLabel;
    private javax.swing.JPanel trainContainer;
    private javax.swing.JLabel trainDocField;
    private javax.swing.JLabel trainDocLabel;
    private javax.swing.JLabel trainModelField;
    private javax.swing.JLabel trainModelLabel;
    private javax.swing.JLabel trainNegativeField;
    private javax.swing.JLabel trainNegativeLabel;
    private javax.swing.JPanel trainPanel;
    private javax.swing.JLabel trainPositiveField;
    private javax.swing.JLabel trainPositiveLabel;
    private javax.swing.JLabel trainTermField;
    private javax.swing.JLabel trainTermLabel;
    private javax.swing.JLabel truePredictField1;
    private javax.swing.JLabel truePredictField2;
    private javax.swing.JLabel truePredictField3;
    private javax.swing.JLabel truePredictField4;
    private javax.swing.JLabel truePredictLabel1;
    private javax.swing.JLabel truePredictLabel2;
    private javax.swing.JLabel truePredictLabel3;
    private javax.swing.JLabel truePredictLabel4;
    // End of variables declaration//GEN-END:variables
}
