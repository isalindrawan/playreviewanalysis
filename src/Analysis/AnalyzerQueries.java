package Analysis;

import ClassModel.ReviewModel;
import Helper.AdditionalQueries;
import Helper.DButils;
import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

public final class AnalyzerQueries {

    private String sentiment_db, user_db;
    private ArrayList<Long> time;

    public AnalyzerQueries(ArrayList<ReviewModel> model, String sentiment_db,
            String user_db, JTextArea area, boolean multi, String file) {

        this.sentiment_db = sentiment_db;
        this.user_db = user_db;
        time = new ArrayList<>();

        clearTables();

        try {

            long start = System.currentTimeMillis();

            if (multi) {

                area.append("Menganalisa file " + file + " ...\n");
                Thread.sleep(500);

                insertApp(model);
                insertReview(model);
                insertModel();
                analysis();
                testPerformace();
                updateApp();

                long end = System.currentTimeMillis();

                area.append("\nAnalisa file " + file + " selesai\n");
                area.append("dalam waktu " + TimeUnit.MILLISECONDS.toSeconds(end - start) + " detik\n");
                Thread.sleep(500);

            } else {

//            if (multi) {
//
//                area.append("Mulai analisa file : " + file + "\n\n");
//                Thread.sleep(500);
//
//            } else {
                area.append("PERSIAPAN ANALISIS\n\n");
                Thread.sleep(500);
//            }

                insertApp(model);
                area.append("[SELESAI] simpan app ke DB\n");
                Thread.sleep(500);

                insertReview(model);
                area.append("[SELESAI] simpan review ke DB\n");
                Thread.sleep(500);

                insertModel();
                area.append("[SELESAI] salin model dari training DB\n\n");
                Thread.sleep(500);

//            if (!multi) {
//
//                area.append("MULAI ANALISA\n\n");
//                Thread.sleep(500);
//            }
                analysis();
                area.append("[SELESAI] menganalisa sentimen\n");
                Thread.sleep(500);

                testPerformace();
                area.append("[SELESAI] uji performa\n\n");
                Thread.sleep(500);

                updateApp();
                Thread.sleep(500);

                long end = System.currentTimeMillis();

//            if (multi) {
//
//                area.append("Analisa pada " + file + " selesai \n");
//                area.append("dalam waktu " + TimeUnit.MILLISECONDS.toSeconds(end - start) + " detik\n\n");
//
//            } else {
                area.append("ANALISIS SELESAI \n");
                area.append("dalam waktu " + TimeUnit.MILLISECONDS.toSeconds(end - start) + " detik\n\n");
//            }
                Thread.sleep(500);
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void clearTables() {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        Statement state = null;

        String[] tables = {"Accuracy", "Model", "Result",
            "Review", "Application"};

        try {

            state = connect.createStatement();

            for (String table : tables) {

                String query = "delete from " + table;

                state.addBatch(query);
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    private ArrayList<Integer> getID(String db_name, String table) {

        DButils utils = new DButils();
        Connection connect = utils.connect(db_name);
        Statement state = null;
        ResultSet set = null;

        ArrayList<Integer> ID = new ArrayList<>();

        try {

            String query = "select ID from " + table;

            state = connect.createStatement();
            set = state.executeQuery(query);

            while (set.next()) {

                ID.add(set.getInt("ID"));
            }

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return ID;

    }

    private HashMap<Integer, ArrayList<String>> getReviewID() {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        Statement state = null;
        ResultSet set = null;

        HashMap<Integer, ArrayList<String>> map = new HashMap<>();
        ArrayList<String> review;

        try {

            String query = "select ID, Review from Review";
            state = connect.createStatement();
            set = state.executeQuery(query);

            while (set.next()) {

                review = new ArrayList<>(Arrays.asList(set.getString("Review").split(" ")));
                map.put(set.getInt("ID"), review);
            }

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return map;
    }

    private Double getProbability(String label, String term, int model_id) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        Double result = 0.0;

        try {

            String query = "select ifnull((\n"
                    + "select P.Log_" + label + " from Term_Probability_Model as TPM\n"
                    + "inner join Term as T on TPM.ID_Term = T.ID\n"
                    + "inner join Probability as P on TPM.ID_Probability = P.ID\n"
                    + "inner join Model as M on TPM.ID_Model = M.ID\n"
                    + "where T.Term = '" + term + "'\n"
                    + "and M.ID = " + model_id + "), 0.0) as Result";

            state = connect.createStatement();
            result = state.executeQuery(query).getDouble("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    private int getConfusion(int model, int before, int after) {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        Statement state = null;

        int result = 0;

        try {

            String query = "select ifnull(count(), 0) as Result from\n"
                    + "(select Review.Label as Before, Result.Label as After from Result\n"
                    + "inner join Review on Result.ID_Review = Review.ID\n"
                    + "where Result.ID_Model = " + model + ")\n"
                    + "where Before = " + before + " and After = " + after;

            state = connect.createStatement();
            result = state.executeQuery(query).getInt("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    private int getTruePrediction(int model) {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        Statement state = null;

        int result = 0;

        try {

            String query = "select ifnull(count(), 0) as Result from\n"
                    + "(select Review.Label as Before, Result.Label as After from Result\n"
                    + "inner join Review on Result.ID_Review = Review.ID\n"
                    + "where Result.ID_Model = " + model + ")\n"
                    + "where Before = After";

            state = connect.createStatement();
            result = state.executeQuery(query).getInt("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    private Double getLogPrior(String label, int model_id) {

        DButils utils = new DButils();
        Connection connect = utils.connect(user_db);
        Statement state = null;

        Double result = 0.0;

        try {

            String query = "select Log_" + label + " as Result from Model where ID = " + model_id;
            state = connect.createStatement();
            result = state.executeQuery(query).getDouble("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    // insert application table
    private void insertApp(ArrayList<ReviewModel> param) {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        PreparedStatement ps = null;

        try {

            // menghapus duplikasi nama aplikasi pada tiap data
            ArrayList<String> name = new ArrayList<>();
            LinkedHashSet<String> set;

            for (ReviewModel temp : param) {

                name.add(temp.getAppname());
            }

            // konversi ke hashset untuk menghapus duplikasi pada arraylist
            // app name
            set = new LinkedHashSet<>(name);
            name = new ArrayList<>(set);

            // inisiasi query
            String query = "insert into Application (ID, Name) values (NULL, ?)";
            ps = connect.prepareStatement(query);

            // menambahkan query untuk tiap nama app, untuk di eksekusi
            // secara bersamaan / batch
            for (String temp : name) {

                ps.setString(1, temp);
                ps.addBatch();
            }

            // execute batch queries
            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }

    private void updateApp() {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        Statement state = null;

        ArrayList<Integer> ID = getID(sentiment_db, "Application");

        try {

            state = connect.createStatement();

            for (int id : ID) {

                String query = "update Application set Total = "
                        + new AdditionalQueries().getRowCount(sentiment_db, "Review", true, "ID_App", String.valueOf(id))
                        + ", Positive = " + new AdditionalQueries().getRowCount(sentiment_db, "Review", true, "ID_App", (String.valueOf(id) + " and Label = 1"))
                        + ", Negative = " + new AdditionalQueries().getRowCount(sentiment_db, "Review", true, "ID_App", (String.valueOf(id) + " and Label = 0"))
                        + " where ID = " + id;

                state.addBatch(query);
            }

            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    // insert review table
    private void insertReview(ArrayList<ReviewModel> param) {

        /*
                menambahkan query insert dengan value review, rate, label
                dan id aplikasi yang di dapat melalui sub query
         */
        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        Statement state = null;

        try {

            state = connect.createStatement();

            for (ReviewModel temp : param) {

                String query
                        = "insert into Review values (NULL, '" + temp.getReviewAfter()
                        + "', " + temp.getRate() + ", " + temp.getLabel()
                        + ", (select ID from Application where Name = '"
                        + temp.getAppname() + "'))";

                state.addBatch(query);
            }

            // execute batch queries
            state.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }
    }

    private void insertModel() {

        DButils utils = new DButils();
        Connection connect_main = utils.connect(sentiment_db);
        PreparedStatement ps = null;

        Connection connect_sub = utils.connect(user_db);
        Statement state = null;
        ResultSet set = null;

        try {

            String sub_query = "select * from Model";
            String main_query = "insert into Model values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            state = connect_sub.createStatement();
            set = state.executeQuery(sub_query);

            ps = connect_main.prepareStatement(main_query);

            while (set.next()) {

                ps.setInt(1, set.getInt("ID"));
                ps.setInt(2, set.getInt("Percent"));
                ps.setInt(3, set.getInt("Docs_Positive"));
                ps.setInt(4, set.getInt("Docs_Negative"));
                ps.setDouble(5, set.getDouble("Prior_Positive"));
                ps.setDouble(6, set.getDouble("Prior_Negative"));
                ps.setDouble(7, set.getDouble("Log_Positive"));
                ps.setDouble(8, set.getDouble("Log_Negative"));
                ps.setInt(9, set.getInt("Freq_Positive"));
                ps.setInt(10, set.getInt("Freq_Negative"));
                ps.setInt(11, set.getInt("Term_Total"));

                ps.addBatch();
            }

            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closePS(ps);
            utils.closeResult(set);
            utils.disconnect(connect_sub);
        }
    }

    private void analysis() {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        PreparedStatement ps = null;

        ArrayList<Integer> model_id = getID(sentiment_db, "Model");
        HashMap<Integer, ArrayList<String>> review = new HashMap<>(getReviewID());

        try {

            String query = "insert into Result values (NULL, ?, ?, ?, ?, ?)";
            ps = connect.prepareStatement(query);

            for (Integer id : model_id) {

                Set set = review.entrySet();
                Iterator iterator = set.iterator();

                long start = System.currentTimeMillis();

                while (iterator.hasNext()) {

                    Map.Entry entry = (Map.Entry) iterator.next();

                    int ID = Integer.parseInt(entry.getKey().toString());
                    ArrayList<String> temp_review = (ArrayList<String>) entry.getValue();

                    Double positive = getLogPrior("Positive", id);
                    Double negative = getLogPrior("Negative", id);

                    int label;

                    for (String term : temp_review) {

                        positive = positive + getProbability("Positive", term, id);
                        negative = negative + getProbability("Negative", term, id);
                    }

                    int compare = Double.compare(positive, negative);

                    if (compare > 0) {

                        label = 1;

                    } else if (compare < 0) {

                        label = 0;

                    } else {

                        label = 3;
                    }

                    positive = BigDecimal.valueOf(positive).setScale(5, RoundingMode.HALF_UP).doubleValue();
                    negative = BigDecimal.valueOf(negative).setScale(5, RoundingMode.HALF_UP).doubleValue();

                    ps.setInt(1, ID);
                    ps.setInt(2, id);
                    ps.setDouble(3, positive);
                    ps.setDouble(4, negative);
                    ps.setInt(5, label);

                    ps.addBatch();
                }

                long end = System.currentTimeMillis();

                time.add(end - start);

                ps.executeBatch();
            }

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }

    private void testPerformace() {

        DButils utils = new DButils();
        Connection connect = utils.connect(sentiment_db);
        PreparedStatement ps = null;

        ArrayList<Integer> model_id = getID(sentiment_db, "Model");
        HashMap<Integer, ArrayList<String>> review = new HashMap<>(getReviewID());

        int counter = 0;

        try {

            String query = "insert into Accuracy values (NULL, ?, ?, ?, ?, ?, ?)";
            ps = connect.prepareStatement(query);

            for (Integer id : model_id) {

                int TP = getConfusion(id, 1, 1);
                int TN = getConfusion(id, 0, 0);
                int FP = getConfusion(id, 0, 1);
                int FN = getConfusion(id, 1, 0);
                int docs = getID(sentiment_db, "Review").size();
                int true_prediction = getTruePrediction(id);

                Double accuracy = (Double.valueOf(true_prediction) / Double.valueOf(docs)) * 100;
                Double precision = (Double.valueOf(TP) / (Double.valueOf(TP) + Double.valueOf(FP))) * 100;
                Double recall = (Double.valueOf(TP) / (Double.valueOf(TP) + Double.valueOf(FN))) * 100;
                Double measure = 2 * ((precision * recall) / (precision + recall));

                accuracy = BigDecimal.valueOf(accuracy).setScale(2, RoundingMode.HALF_UP).doubleValue();
                precision = BigDecimal.valueOf(precision).setScale(2, RoundingMode.HALF_UP).doubleValue();
                recall = BigDecimal.valueOf(recall).setScale(2, RoundingMode.HALF_UP).doubleValue();
                measure = BigDecimal.valueOf(measure).setScale(2, RoundingMode.HALF_UP).doubleValue();

                ps.setInt(1, id);
                ps.setDouble(2, accuracy);
                ps.setDouble(3, precision);
                ps.setDouble(4, recall);
                ps.setDouble(5, measure);
                ps.setLong(6, time.get(counter));

                ps.addBatch();

                counter++;
            }

            ps.executeBatch();

        } catch (SQLException ex) {

            Logger.getLogger(AnalyzerQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closePS(ps);
            utils.disconnect(connect);
        }
    }
}
