package PreProcess;

import ClassModel.ReviewModel;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class ProcessCSV extends Thread {

    private ArrayList<ReviewModel> dataset;
    private ArrayList<String> review;
    private final File file;
    private Reader reader;

    public ProcessCSV(File file) {

        /*
            konversi file csv ke dalam bentuk arraylist class
            dengan menggunakan library openCSV
         */
        this.file = file;
        dataset = new ArrayList<>();
        reader = null;

        try {

            reader = Files.newBufferedReader(Paths.get(file.getAbsolutePath()));
            CSVParser parse = new CSVParser(reader, CSVFormat.newFormat(';'));

            // store all records to temporary variables
            for (CSVRecord record : parse) {

                // get app name from csv file
                String appname = record.get(0);

                //get review from csv file and do text Preprocessing
                String review_before = record.get(1);

                // get rate from csv
                int rate = Integer.parseInt(record.get(2));

                // initiate label
                int label = 0;

                // labeling review
                switch (rate) {

                    case 1:
                        label = 0;
                        break;
                    case 2:
                        label = 0;
                        break;
                    case 4:
                        label = 1;
                        break;
                    case 5:
                        label = 1;
                        break;
                }

                // store review data to arraylist
                dataset.add(new ReviewModel(appname, review_before, rate, label));

            }

        } catch (IOException ex) {

            ex.getStackTrace();

        } finally {

            try {

                //bar.setIndeterminate(false);
                reader.close();

            } catch (IOException ex) {

                ex.getStackTrace();
            }
        }
    }

    public ArrayList<ReviewModel> getArrayList() {

        return dataset;
    }
}
