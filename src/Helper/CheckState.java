/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.io.File;

/**
 *
 * @author isalindrawan
 */
public class CheckState {

    private boolean status = true;

    public CheckState() {

        String folder = "./temp"; // Give your folderName
        File[] files = new File(folder).listFiles();

        for (int i = 0; i < files.length; i++) {

            if (files[i].isFile()) {

                String name = files[i].getName();

                if (name.startsWith("lock_") && name.endsWith(".file")) {

                    status = true;
                    break;

                } else {

                    status = false;
                }
            }
        }
    }

    public boolean getStatus() {

        return status;
    }
}
