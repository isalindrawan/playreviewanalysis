package TrainingClassifier;

import PreProcess.ProcessCSV;
import PreProcess.Preprocessing;
import PreProcess.StringOperation;
import ClassModel.ReviewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JTextArea;

public class TrainData {

    private File trainingCSV;

    private ArrayList<ReviewModel> model;
    private ArrayList<String> term;
    private ArrayList<HashMap<String, Integer>> termCount;

    public TrainData(File trainingCSV, String user_db, ArrayList<Integer> limit, JTextArea area) {

        this.trainingCSV = trainingCSV;

        preTrain();

        TrainQueries trainQueries = new TrainQueries(model, term, termCount, limit, user_db, area);

    }

    private void preTrain() {

        import_csv();

        preprocess();

        preDBOperation();
    }

    private void import_csv() {

        // konversi file csv menjadi arraylist class
        model = new ProcessCSV(trainingCSV).getArrayList();
    }

    private void preprocess() {

        /*
            preprocessing dokumen review yang ada pada arraylist class
            selanjutnya di masukkan kembali kedalam arraylist asal
         */
        Preprocessing preprocess = new Preprocessing();
        for (int i = 0; i < model.size(); i++) {

            model.get(i).setReview(
                    new ArrayList<>(preprocess.analyze(
                            model.get(i).getReviewBefore().replaceAll("[^a-zA-Z\\s+]", " ")
                    ))
            );
        }
    }

    private void preDBOperation() {

        /*
            melakukan persiapan sebelum dilakukan operasi insert DB
            dengan menghitung masing masing total kata yang terdapat
            pada tiap review contoh : aku - 5, baik - 2

            output : daftar kata / term pada seluruh dokumen, dan jumlah pada
            masing-masing dokumen
         */
        termCount = new ArrayList<>();
        term = new ArrayList<>(new StringOperation(model).generateSingleTerm());

        model.forEach((temp) -> {

            termCount.add(new StringOperation().countTermInReview(temp.getReview()));
        });
    }
}
