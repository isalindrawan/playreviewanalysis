/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import ClassModel.ModeModel;
import ClassModel.ProbabilityModel;
import ClassModel.ResultModel;
import ClassModel.TermModel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isalindrawan
 */
public class AdditionalQueries {

    public int getRowCount(String training_db, String table, boolean expand,
            String condition, String value) {

        DButils utils = new DButils();
        Connection connect = utils.connect(training_db);
        Statement state = null;

        int result = 0;

        try {

            String query = "select ifnull(count(), 0) as Result from " + table;

            if (expand) {

                query = query + " where " + condition + " = " + value;
            }

            state = connect.createStatement();
            result = state.executeQuery(query).getInt("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    public Double getAccuracy(String db, String field, int model) {

        DButils utils = new DButils();
        Connection connect = utils.connect(db);
        Statement state = null;

        Double result = 0.0;

        try {

            String query = "select " + field + " as Result from Accuracy where ID_Model = " + model;
            state = connect.createStatement();

            result = state.executeQuery(query).getDouble("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    public int getCountPrediction(String db, int model, boolean predicted) {

        DButils utils = new DButils();
        Connection connect = utils.connect(db);
        Statement state = null;

        int result = 0;
        String operator = "=";

        if (!predicted) {

            operator = "!=";
        }

        try {

            String query = "select ifnull(count(), 0) as Result from Result \n"
                    + "inner join Review on Result.ID_Review = Review.ID \n"
                    + "where Review.Label " + operator + " Result.Label \n"
                    + "and Result.ID_Model = " + model;

            state = connect.createStatement();

            result = state.executeQuery(query).getInt("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    public int getCountPredictionLabel(String db, int model, int predicted) {

        DButils utils = new DButils();
        Connection connect = utils.connect(db);
        Statement state = null;

        int result = 0;

        try {

            String query = "select ifnull(count(), 0) as Result from Result \n"
                    + "where Label = " + predicted
                    + " and ID_Model = " + model;

            state = connect.createStatement();

            result = state.executeQuery(query).getInt("Result");

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.disconnect(connect);
        }

        return result;
    }

    public ArrayList<ResultModel> getResultDetail(String db, int id) {

        DButils utils = new DButils();
        Connection connect = utils.connect(db);
        Statement state = null;
        ResultSet set = null;

        ArrayList<ResultModel> resultModel = new ArrayList<>();

        try {

            String query = "select Review.Review, Result.Negative as 'Negatif', Result.Positive as 'Positif', \n"
                    + "(case when Review.Label = 1 then 'Positif' when Review.Label = 0 then 'Negatif' END) as 'Label', \n"
                    + "(case when Result.Label = 1 then 'Positif' when Result.Label = 0 then 'Negatif' END) as 'Prediksi' \n"
                    + "from Result\n"
                    + "inner join Review on Result.ID_Review = Review.ID\n"
                    + "where Result.ID_Model = " + id;

            state = connect.createStatement();

            set = state.executeQuery(query);

            while (set.next()) {

                ResultModel model = new ResultModel(
                        set.getString("Review"), set.getDouble("Negatif"), set.getDouble("Positif"),
                        set.getString("Label"), set.getString("Prediksi")
                );

                resultModel.add(model);
            }

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return resultModel;
    }

    public ModeModel getModel(String db, String percentage) {

        DButils utils = new DButils();
        Connection connect = utils.connect(db);
        Statement state = null;
        ResultSet set = null;

        ModeModel model = null;

        try {

            String query = "select * from Model where Percent = " + percentage;

            state = connect.createStatement();

            set = state.executeQuery(query);

            model = new ModeModel(
                    set.getInt("Docs_Positive"), set.getInt("Docs_Negative"),
                    set.getInt("Freq_Positive"), set.getInt("Freq_Negative"),
                    set.getInt("Term_Total"), set.getDouble("Prior_Positive"),
                    set.getDouble("Prior_Negative"), set.getDouble("Log_Positive"),
                    set.getDouble("Log_Negative")
            );

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return model;
    }

    public ArrayList<ProbabilityModel> getProbability(String training_db, String percentage) {

        DButils utils = new DButils();
        Connection connect = utils.connect(training_db);
        Statement state = null;
        ResultSet set = null;

        ArrayList<ProbabilityModel> prob = new ArrayList<>();

        try {

            String query = "select T.Term, P.* from Term_Probability_Model as TPM\n"
                    + "inner join Term as T on T.id = TPM.ID_Term\n"
                    + "inner join Model as M on M.ID = TPM.ID_Model\n"
                    + "inner join Probability as P on P.ID = TPM.ID_Probability\n"
                    + "where M.Percent = " + percentage;

            state = connect.createStatement();

            set = state.executeQuery(query);

            while (set.next()) {

                ProbabilityModel model = new ProbabilityModel(
                        set.getString("Term"), set.getInt("Freq_Positive"),
                        set.getInt("Freq_Negative"), set.getDouble("Prob_Positive"),
                        set.getDouble("Prob_Negative"), set.getDouble("Log_Positive"),
                        set.getDouble("Log_Negative")
                );

                prob.add(model);
            }

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return prob;
    }

    public ArrayList<TermModel> getTermDetail(String training_db) {

        DButils utils = new DButils();
        Connection connect = utils.connect(training_db);
        Statement state = null;
        ResultSet set = null;

        ArrayList<TermModel> term = new ArrayList<>();

        try {

            String query = "select T.Term as Term, P.Freq_Positive, P.Freq_Negative, C.Positive, C.Negative, C.Score\n"
                    + "from Term_Probability_Model as TPM\n"
                    + "inner join Term as T on T.ID = TPM.ID_Term\n"
                    + "inner join Probability as P on P.ID = TPM.ID_Probability\n"
                    + "inner join Chi as C on T.ID_Chi = C.ID\n"
                    + "where TPM.ID_Model = 3";

            state = connect.createStatement();

            set = state.executeQuery(query);

            while (set.next()) {

                TermModel model = new TermModel(
                        set.getString("Term"), set.getInt("Freq_Positive"),
                        set.getInt("Freq_Negative"), set.getDouble("Positive"),
                        set.getDouble("Negative"), set.getDouble("Score")
                );

                term.add(model);
            }

        } catch (SQLException ex) {

            Logger.getLogger(AdditionalQueries.class.getName()).log(Level.SEVERE, null, ex);

        } finally {

            utils.closeStatement(state);
            utils.closeResult(set);
            utils.disconnect(connect);
        }

        return term;
    }
}
