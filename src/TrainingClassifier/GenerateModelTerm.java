package TrainingClassifier;

import java.util.ArrayList;
import java.util.HashMap;

public class GenerateModelTerm {

    private ArrayList<Integer> count;
    private HashMap<Integer, ArrayList<Integer>> modelTerm;

    public GenerateModelTerm(ArrayList<Integer> param, String user_db) {

        /*
            mendapatkan semua id kata yang tergolong / digunakan sebagai training model
            berdasarkan prosentase 100%, 75%, 50% dari total seluruh data dengan nilai chi square tertinggi
            contoh output
            {100, [1,3,5,7,9]}
            {75, [1,3,5,7]}
         */
        int termCount = new TrainQueries(user_db).getCount("Term", null);

        count = new ArrayList<>();
        modelTerm = new HashMap<>();

        param.forEach((temp) -> {

            Double limit = (Double.valueOf(temp) / 100) * termCount;
            count.add(limit.intValue());
            modelTerm.put(temp, new ArrayList<>(new TrainQueries(user_db).getCSTermID(limit.intValue())));
        });
    }

    public HashMap<Integer, ArrayList<Integer>> getModelTerm() {

        return modelTerm;
    }

    public ArrayList<Integer> getTermLimit() {

        return count;
    }
}
